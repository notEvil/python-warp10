import mdformat_mytables
import lark
import lxml.html as l_html
import pypandoc
import selenium.webdriver as s_webdriver
import selenium.webdriver.common.by as swc_by
import zstandard
import collections.abc as c_abc
import dataclasses
import functools
import pathlib
import pickle
import pprint
import re
import sys
import typing


## Python utilities


class NONE:
    pass


def all_equal(objects: c_abc.Iterable) -> bool:
    objects = iter(objects)
    try:
        first_object = next(objects)

    except StopIteration:
        return True

    return all(object == first_object for object in objects)


def print_stderr(*args, file=sys.stderr, **kwargs):
    return print(*args, file=file, **kwargs)


def pprint_stderr(*args, stream=sys.stderr, **kwargs):
    return pprint.pprint(*args, stream=stream, **kwargs)


class Object:
    def __init__(self, **kwargs):
        super().__init__()

        for name, object in kwargs.items():
            setattr(self, name, object)


## cached


class _Cached:
    def __init__(self, attribute_name: str):
        super().__init__()

        self.attribute_name = attribute_name

    def __call__(self, function: c_abc.Callable) -> c_abc.Callable:
        attribute_name = self.attribute_name

        @functools.wraps(function)
        def _cached_function(*args, **kwargs):
            self, *args = args

            tuple = getattr(self, attribute_name)
            if tuple is not NONE:
                cached_args, cached_kwargs, cached_object = tuple
                if cached_args == args and cached_kwargs == kwargs:
                    if isinstance(cached_object, _CachedException):
                        raise cached_object.exception

                    return cached_object

            try:
                object = function(self, *args, **kwargs)

            except Exception as exception:
                _v_ = (args, kwargs, _CachedException(exception))
                setattr(self, attribute_name, _v_)

                raise

            setattr(self, attribute_name, (args, kwargs, object))
            return object

        return _cached_function


class _CachedException:
    def __init__(self, exception):
        super().__init__()

        self.exception = exception


cached = _Cached


## skip


class Skip(Exception):
    pass


T = typing.TypeVar("T")


def skip(
    function: c_abc.Callable[[T], typing.Literal[False] | typing.Any],
    false_function: (
        c_abc.Callable[[T, Skip], typing.Any] | typing.Literal[True] | None
    ) = None,
) -> c_abc.Callable[[T], bool]:
    def _skip(object):
        try:
            object = function(object)

        except Skip as skip:
            if false_function is not None:
                try:
                    if false_function is True:
                        reason_string = str(skip)
                        if reason_string == "":
                            reason_string = "unknown"

                        print_stderr(f"skipping {object}: {reason_string}")

                    else:
                        false_function(object, skip)

                except Skip:
                    pass

            return False

        return object is not False

    return _skip


## Selenium utilities


def xpath(object, path_string):
    if object is None:
        return []

    return object.find_elements(swc_by.By.XPATH, path_string)


def tag(object, tag_name):
    if object is None:
        return []

    return object.find_elements(swc_by.By.TAG_NAME, tag_name)


def class_(name):
    return f'contains(concat(" ", normalize-space(@class), " "), " {name} ")'


def first(object):
    return next(iter(object), None)


## scrape functions from warp10.io/doc/reference


_SIGNATURE_COLUMNS = ["Stack", "Name", "Type", "Description"]
_PICKLE_PATH = pathlib.Path("./generate_code")


_web_driver = None


def _get_web_driver():
    global _web_driver

    if _web_driver is not None:
        return _web_driver

    _web_driver = s_webdriver.Firefox()
    _web_driver.implicitly_wait(10)
    return _web_driver


def _get_url_strings():
    _get_web_driver().get("https://warp10.io/doc/reference")
    xpath(_get_web_driver(), '//a[@href="/doc/HTTP"]')  # wait for page load

    _v_ = xpath(_get_web_driver(), '//li/a[starts-with(@href, "/doc/")]')
    return [element.get_attribute("href") for element in _v_]


def _get_function_dictionaries():
    if _PICKLE_PATH.exists():
        with zstandard.open(_PICKLE_PATH, "rb") as file:
            function_dictionaries = pickle.load(file)

    else:
        function_dictionaries = {}

    try:
        for url_string in _get_url_strings():
            if url_string in function_dictionaries:
                continue

            print_stderr()

            _get_web_driver().get(typing.cast(str, url_string))

            for _ in range(3):
                _v_ = first(xpath(_get_web_driver(), "//h1")).get_attribute("innerText")
                name = _v_

                if name != "":
                    break

            else:
                raise Exception

            print_stderr("name", repr(name))

            _v_ = xpath(_get_web_driver(), '//a[starts-with(@href, "/doc/tags/")]')
            tag_names = [
                typing.cast(str, element.get_attribute("href")).split("/")[-1]
                for element in _v_
            ]

            print_stderr("tags", repr(tag_names))

            _v_ = xpath(_get_web_driver(), '//span[starts-with(text(), "Since v")]')
            element = first(_v_)

            _v_ = None if element is None else element.text.removeprefix("Since v")
            version_string = _v_

            print_stderr("version", repr(version_string))

            version_class_string = class_("version")
            mt3_class_string = class_("mt-3")

            _v_ = (
                f"//*[{version_class_string}]/following-sibling::*[{mt3_class_string}]"
            )
            _v_ = first(xpath(_get_web_driver(), _v_)).get_attribute("outerHTML")
            description_html_string = l_html.tostring(l_html.fromstring(_v_))

            print_stderr("description")
            print_stderr(description_html_string)

            _v_ = pypandoc.convert_text(description_html_string, "gfm", format="html")
            description_markdown_string = _v_.strip()

            print_stderr("description markdown")
            print_stderr(description_markdown_string)

            class_string = class_("sigsumup")
            signature_strings = [
                " ".join(
                    l_html.fromstring(element.get_attribute("outerHTML")).itertext()
                ).split()
                for element in xpath(_get_web_driver(), f"//div[{class_string}]")
            ]

            print_stderr("signatures")
            pprint_stderr(signature_strings)

            signature_tables = []

            for table_element in xpath(_get_web_driver(), "//table"):
                _v_ = xpath(table_element, "./thead/tr/th")
                column_names = [element.get_attribute("innerText") for element in _v_]

                if column_names != _SIGNATURE_COLUMNS:
                    continue

                _v_ = [
                    [
                        element.get_attribute("innerText")
                        for element in tag(row_element, "td")
                    ]
                    for row_element in xpath(table_element, "./tbody/tr")
                ]
                signature_tables.append(_v_)

            assert len(signature_tables) == len(signature_strings)

            for signature_table in signature_tables:
                pprint_stderr([_SIGNATURE_COLUMNS] + signature_table)

            _v_ = zip(signature_strings, signature_tables)
            _v_ = [dict(strings=strings, table=table) for strings, table in _v_]
            function_dictionaries[url_string] = dict(
                url=url_string,
                name=name,
                tags=tag_names,
                version=version_string,
                description=description_html_string,
                description_markdown=description_markdown_string,
                signatures=_v_,
            )

    finally:
        with zstandard.open(_PICKLE_PATH, "wb") as file:
            pickle.dump(function_dictionaries, file)

    return function_dictionaries


## generate code


_NAMES = {
    "!": "_logical_not",
    "!=": "_ne",
    "%": "_mod",
    "&": "_and",
    "&&": "_logical_and",
    "*": "_mul",
    "**": "_pow",
    "+!": "_collection_add",
    "+": "_add",
    "-": "_sub",
    "/": "_truediv",
    "<": "_lt",
    "<<": "_lshift",
    "<=": "_le",
    "==": "_eq",
    ">": "_gt",
    ">=": "_ge",
    ">>": "_rshift",
    ">>>": "_shift_right",
    "^": "_xor",
    "|": "_or",
    "||": "_logical_or",
    "~": "_invert",
    "~=": "_approximate_equal",
    "as": "as_",
    "class": "class_",
    "else": "else_",
    "finally": "finally_",
    "from": "from_",
    "if": "if_",
    "lambda": "lambda_",
    "try": "try_",
}


def _get_name(string):
    if string in ("(", "()", ")", "[", "[]", "]", "[[", "[[]]", "]]", "{", "{}", "}"):
        return

    name = _NAMES.get(string)
    if name is not None:
        return name

    if string.startswith("->"):
        string = f"TO_{string[2:]}"

    if string.endswith("->"):
        string = f"FROM_{string[:-2]}"

    string = string.removesuffix("*")
    return re.sub(r"\W", "_", string)


_TYPE_PARSER = lark.Lark(r"""
start: star | plus | type
star: type "*"
plus: type "+"
type: list | set | NAME
list: "LIST<" type ">"
set: "SET<" type ">"
NAME: /\w+/
""")


class _TypeTransformer(lark.Transformer):
    def NAME(self, object):
        name = str(object)

        _v_ = name in (
            "BOOLEAN",
            "LONG",
            "DOUBLE",
            "NUMBER",
            "STRING",
            "LIST",
            "SET",
            "MAP",
            "ANY",
        )
        _v_ = _Type(f"_{name[:1]}{name[1:].lower()}" if _v_ else f"_{name}", f"_{name}")
        return _v_

    def list(self, types):
        (type,) = types

        _v_ = f"_List[{type.argument_string}, {type.return_string}]"
        return _Type(_v_, f"_LIST[{type.return_string}]")

    def set(self, types):
        (type,) = types

        _v_ = f"_Set[{type.argument_string}, {type.return_string}]"
        return _Type(_v_, f"_SET[{type.return_string}]")

    def type(self, types):
        (type,) = types
        return type

    def star(self, types):
        (type,) = types
        return _Type(f"Star[{type.argument_string}]", f"Star[{type.return_string}]")

    def plus(self, types):
        (type,) = types
        return _Type(f"Plus[{type.argument_string}]", f"Plus[{type.return_string}]")

    def start(self, types):
        (type,) = types
        return type


@dataclasses.dataclass
class _Type:
    argument_string: str
    return_string: str


def _get_type_string(string, is_argument):
    type = _TypeTransformer().transform(_TYPE_PARSER.parse(string))
    return type.argument_string if is_argument else type.return_string


class _Function:
    def __init__(self, dictionary, names, composite_types):
        super().__init__()

        self.dictionary = dictionary
        self.names = names
        self.composite_types = composite_types

        self._name = NONE
        self._overloads = NONE
        self._code_string = NONE
        self._description_doc_string = NONE

    @cached("_name")
    def get_name(self):
        name = _get_name(self.dictionary["name"])
        if name is None:
            raise Skip("name")

        assert name not in self.names
        self.names.add(name)
        return name

    @cached("_overloads")
    def get_overloads(self):
        signature_dictionaries = self.dictionary["signatures"]
        if len(signature_dictionaries) == 0:  # constant
            signature_dictionaries = [dict(strings=[self.dictionary["name"]])]

        overloads = []

        for signature_dictionary in signature_dictionaries:
            overload = _Overload(signature_dictionary, self)
            signature_strings = signature_dictionary["strings"]
            optional_arguments = [
                argument
                for argument in _Overload(signature_dictionary, self).get_arguments()
                if not argument.is_kwarg
                and signature_strings[argument.string_index].endswith("*")
            ]
            if len(optional_arguments) == 0:
                overloads.append(overload)
                continue

            for length in range(len(optional_arguments) + 1):
                overload_strings = list(signature_strings)
                overload_table = list(signature_dictionary["table"])

                for argument in reversed(optional_arguments[length:]):
                    del overload_strings[argument.get_slice()]
                    for index, strings in enumerate(overload_table):
                        _v_ = Object(**dict(zip(_SIGNATURE_COLUMNS, strings))).Name
                        if _v_ == signature_strings[argument.string_index]:
                            break
                    else:
                        raise Exception

                    del overload_table[index]

                _v_ = dict(strings=overload_strings, table=overload_table)
                overloads.append(_Overload(_v_, self))

        if len(overloads) == 0:
            raise Skip("no overload")

        return overloads

    @cached("_code_string")
    def get_code_string(self):
        overloads = self.get_overloads()

        if len(overloads) == 1:
            (overload,) = overloads
            args_arguments, kwargs_arguments = overload.get_arguments_tuple()

            _v_ = (argument.get_python_name() for argument in args_arguments)
            args_string = self._get_tuple_string(_v_)

            _v_ = self._get_dictionary_string((name, name) for name in kwargs_arguments)
            kwargs_string = _v_

            strings_string = self._get_strings_string(overload.get_template_strings())
            args_indices_string = self._get_args_indices_string(args_arguments)
            kwargs_indices_string = self._get_kwargs_indices_string(kwargs_arguments)

            _v_ = f"({strings_string}, {args_indices_string}, {kwargs_indices_string})"
            template_tuple_string = _v_

            warp10_name = self.dictionary["name"]
            return f"""
{overload.get_code_string()}
    return typing.cast({overload.get_return_type_string()}, warp10._get_string({args_string}, {kwargs_string}, [{template_tuple_string}], {self.get_name()}))
def {self.get_name()}_stack(*args) -> {overload.get_return_type_string()}:{overload.get_documentation_string()}
    return typing.cast({overload.get_return_type_string()}, warp10._stack(args, {repr(warp10_name)}, {overload.get_return_type_string()}))
"""[1:-1]

        _v_ = sorted(overloads, key=lambda overload: overload.get_sort_key())
        overloads_string = "\n".join(f"""
@typing.overload
{overload.get_code_string()}
    ...
"""[1:-1] for overload in _v_)

        # create template tuples
        slice_tuples = {}
        template_tuple_strings = []

        for overload in sorted(overloads, key=lambda overload: overload.get_sort_key()):
            template_strings = overload.get_template_strings()
            args_arguments, kwargs_arguments = overload.get_arguments_tuple()

            # check for collisions
            _v_ = kwargs_arguments.items()
            _v_ = {name: argument.get_slice() for name, argument in _v_}
            slice_tuple = ([argument.get_slice() for argument in args_arguments], _v_)

            length = len(slice_tuples)

            _v_ = slice_tuples.setdefault(tuple(template_strings), slice_tuple)
            assert _v_ == slice_tuple

            if len(slice_tuples) == length:  # known
                continue

            strings_string = self._get_strings_string(template_strings)
            args_indices_string = self._get_args_indices_string(args_arguments)
            kwargs_indices_string = self._get_kwargs_indices_string(kwargs_arguments)

            _v_ = f"({strings_string}, {args_indices_string}, {kwargs_indices_string})"
            template_tuple_strings.append(_v_)

        template_tuples_string = ", ".join(template_tuple_strings)

        # create stack variant
        _v_ = {overload.get_return_type_string() for overload in overloads}
        return_type_strings = _v_

        warp10_name = self.dictionary["name"]
        if len(return_type_strings) == 1:
            (return_type_string,) = return_type_strings
            stack_string = f'''
def {self.get_name()}_stack(*args) -> {return_type_string}:
    r"""
{self._get_description_doc_string()}
    """
    return typing.cast({return_type_string}, warp10._stack(args, {repr(warp10_name)}, {return_type_string}))
'''[1:-1]

        else:
            string = "\n".join(sorted(f"""
@typing.overload
def {self.get_name()}_stack(*args, type) -> {return_type_string}:
    ...
"""[1:-1] for return_type_string in return_type_strings))

            stack_string = f'''
{string}
def {self.get_name()}_stack(*args, type):
    r"""
{self._get_description_doc_string()}
    """
    return typing.cast(typing.Any, warp10._stack(args, {repr(warp10_name)}, type))
'''

        return f"""
{overloads_string}
def {self.get_name()}(*args, **kwargs):
    return typing.cast(typing.Any, warp10._get_string(args, kwargs, [{template_tuples_string}], {self.get_name()}))
{stack_string}
"""[1:-1]

    def _get_tuple_string(self, strings):
        objects_string = ", ".join(strings)
        return "()" if objects_string == "" else f"({objects_string},)"

    def _get_dictionary_string(self, strings):
        _v_ = (f"{key_string}={value_string}" for key_string, value_string in strings)
        items_string = ", ".join(_v_)

        return "{}" if items_string == "" else f"dict({items_string})"

    def _get_strings_string(self, strings):
        _v_ = ", ".join("N" if object is None else repr(object) for object in strings)
        string = _v_

        return f"[{string}]"

    def _get_args_indices_string(self, arguments):
        return repr([argument.get_slice().start for argument in arguments])

    def _get_kwargs_indices_string(self, arguments):
        return self._get_dictionary_string(
            (name, argument.get_slice().start) for name, argument in arguments.items()
        )

    @cached("_description_doc_string")
    def _get_description_doc_string(self):
        tags_string = ", ".join(sorted(self.dictionary["tags"]))
        version_string = self.dictionary["version"]
        version_string = "unknown" if version_string is None else f"v{version_string}"
        url_string = self.dictionary["url"]

        _v_ = self.dictionary["description_markdown"]
        _v_ = re.sub(r'^<div class="mt-3" [^>]+>\n', "", _v_).removesuffix("\n</div>")
        _v_ = mdformat_mytables.convert_tables(_v_, width=75).strip()
        description_string = _v_.replace("\n", "\n    ")

        return f"""
    Tags  | {tags_string}
    Since | {version_string}
    URL   | {url_string}

    {description_string}
"""[1:-1]

    def __str__(self):
        name = self.dictionary["name"]
        return f"<Function {repr(name)}>"


class _Overload:
    def __init__(self, dictionary, function):
        super().__init__()

        self.dictionary = dictionary
        self.function = function

        self._object = NONE

        self._method_name = NONE
        self._code_string = NONE

        self._signature_doc_string = NONE

    def get_arguments(self):
        return self._initialize().arguments

    def get_name_index(self):
        return self._initialize().name_index

    def get_template_strings(self):
        return self._initialize().template_strings

    def get_return_type_string(self):
        return self._initialize().return_type_string

    @cached("_object")
    def _initialize(self):
        signature_strings = list(self.dictionary["strings"])
        string_index = 0
        in_dictionary = False
        in_dictionary_list = []
        arguments = []
        name_index = None

        # parse arguments
        while True:
            signature_string = signature_strings[string_index]
            if signature_string == self.function.dictionary["name"]:
                name_index = string_index
                string_index += 1
                break

            if signature_string in ("[", "(", "{"):
                in_dictionary_list.append(in_dictionary)
                in_dictionary = signature_string == "{"
                string_index += 1

            elif signature_string in ("]", ")", "}"):
                in_dictionary = in_dictionary_list.pop()
                string_index += 1

            else:
                if in_dictionary:
                    signature_strings[string_index] = f"'{signature_string}'"
                    signature_strings[string_index + 1] = None

                else:
                    signature_strings[string_index : string_index + 2] = [None, None]

                arguments.append(_Argument(string_index, in_dictionary, self))
                string_index += 2

        # parse return
        tuples = []
        tuples_list = []
        self.function.composite_types.begin()

        while True:
            if string_index == len(signature_strings):
                break

            signature_string = signature_strings[string_index]
            assert signature_string not in ("(", ")")

            if "," in signature_string:  # ignore
                string_index += 1
                continue

            if signature_string in ("[", "{"):
                in_dictionary_list.append(in_dictionary)
                tuples_list.append(tuples)

                in_dictionary = signature_string == "{"
                tuples = []

                string_index += 1

            elif signature_string in ("]", "}"):
                composite_types = self.function.composite_types
                type_string = (
                    composite_types.get_list_name
                    if signature_string == "]"
                    else composite_types.get_dictionary_name
                )(tuples)

                in_dictionary = in_dictionary_list.pop()
                tuples = tuples_list.pop()

                tuples.append((type_string[1:], type_string))

                string_index += 1

            else:
                _v_ = _get_type_string(signature_strings[string_index + 1], False)
                tuples.append((signature_string, _v_))

                string_index += 2

        if len(tuples) == 0:
            return_type_string = None

        elif len(tuples) == 1:
            self.function.composite_types.commit()
            ((_, return_type_string),) = tuples

        else:
            self.function.composite_types.rollback()
            return_type_string = self.function.composite_types.get_stack_name(tuples)

        # fix name collisions
        counts = {}
        names = {}

        for argument in arguments:
            name = argument.get_python_name()
            count = counts.get(name, 0) + 1
            counts[name] = count
            names[argument] = f"{name}_{count}"

        for argument, name in names.items():
            if counts[argument.get_python_name()] == 1:
                continue

            argument._python_name = ([], {}, name)  # TODO cached api

        return Object(
            arguments=arguments,
            name_index=name_index,
            template_strings=signature_strings[: name_index + 1],
            return_type_string=return_type_string,
        )

    def get_arguments_tuple(self):
        args_arguments = []
        kwargs_arguments = {}
        for argument in self.get_arguments():
            if argument.is_kwarg:
                kwargs_arguments[argument.get_python_name()] = argument

            else:
                args_arguments.append(argument)

        return (args_arguments, kwargs_arguments)

    @cached("_code_string")
    def get_code_string(self, method_name=None, args_indices=None):
        args_arguments, kwargs_arguments = self.get_arguments_tuple()
        argument_strings = [
            argument.get_code_string(type_as_string=method_name is not None)
            for argument in args_arguments
        ]

        if method_name is not None:
            if args_indices is None:
                argument_strings[0] = "self"

            else:
                self_index = args_indices.index(0)
                argument_strings = [
                    argument_strings[index]
                    for index in args_indices
                    if index != self_index
                ]
                argument_strings.insert(0, "self")

        if len(args_arguments) != 0:
            argument_strings.append("/")

        if len(kwargs_arguments) != 0:
            argument_strings.append("*")

        argument_strings.extend(
            argument.get_code_string(type_as_string=method_name is not None)
            for argument in kwargs_arguments.values()
        )

        arguments_string = ", ".join(argument_strings)
        return_type_string = self.get_return_type_string()
        return (
            f"def {self.function.get_name() if method_name is None else method_name}({arguments_string})"
            f" -> {return_type_string if method_name is None else repr(return_type_string)}:{self.get_documentation_string()}"
        )

    def get_documentation_string(self):
        return f'''

    r"""
{self.function._get_description_doc_string()}{self._get_signature_doc_string()}
    """
'''[1:-1]

    @cached("_signature_doc_string")
    def _get_signature_doc_string(self):
        table = self.dictionary.get("table", [])

        _v_ = (Object(**dict(zip(_SIGNATURE_COLUMNS, strings))) for strings in table)
        row_objects = _v_

        strings = []
        row_object = next(row_objects, None)

        if row_object is not None and len(self.get_arguments()) != 0:
            strings.append("""
Parameters
----------
"""[1:-1])
            while True:
                strings.append(f"""
{row_object.Name} : {row_object.Type}
    {row_object.Description}
"""[1:-1])
                row_object = next(row_objects, None)
                if row_object is None:
                    break

                if row_object.Stack == "[TOP]":
                    break

        if row_object is not None:
            if len(strings) != 0:
                strings.append("")

            strings.append("""
Returns
-------
"""[1:-1])
            while True:
                strings.append(f"""
{row_object.Name} : {row_object.Type}
    {row_object.Description}
"""[1:-1])
                row_object = next(row_objects, None)
                if row_object is None:
                    break

        if len(strings) == 0:
            return ""

        string = "\n".join(strings).replace("\n", "\n    ")
        return f"""


    {string}
"""[1:-1]

    def get_sort_key(self):
        return (len(self.get_arguments()), self.get_code_string())

    def __str__(self):
        strings = self.dictionary["strings"]
        return f"<Overload {repr(strings)}>"


class _Argument:
    def __init__(self, string_index, is_kwarg, overload):
        super().__init__()

        self.string_index = string_index
        self.is_kwarg = is_kwarg
        self.overload = overload

        self._python_name = NONE
        self._type_string = NONE

    @cached("_python_name")
    def get_python_name(self):
        return _get_name(self.overload.dictionary["strings"][self.string_index])

    @cached("_type_string")
    def get_type_string(self, as_string=False):
        _v_ = self.overload.dictionary["strings"][self.string_index + 1]
        type_string = _get_type_string(_v_, True)

        return repr(type_string) if as_string else type_string

    def get_slice(self):
        _v_ = self.string_index + 1 if self.is_kwarg else self.string_index
        return slice(_v_, self.string_index + 2)

    def get_code_string(self, type_as_string=False):
        optional_string = " | None = None" if self.is_kwarg else ""

        _v_ = self.get_type_string(as_string=type_as_string)
        return f"{self.get_python_name()}: {_v_}{optional_string}"


class _CompositeTypes:
    def __init__(self):
        super().__init__()

        self._list_names = {}
        self._stack_names = {}
        self._dictionary_names = {}
        self._stack = []

    def get_list_name(self, tuples):
        return self._get_name(self._list_names, "list", tuples)

    def get_stack_name(self, tuples):
        return self._get_name(self._stack_names, "stack", tuples)

    def get_dictionary_name(self, tuples):
        return self._get_name(self._dictionary_names, "dict", tuples)

    def _get_name(self, dictionary, base_name, tuples):
        _tuple = tuple(tuples)
        name = dictionary.get(_tuple)
        if name is not None:
            return name

        name = f"_{base_name}_{len(dictionary) + 1}"
        dictionary[_tuple] = name
        return name

    def begin(self):
        _v_ = self._dictionary_names.copy()
        self._stack.append((self._list_names.copy(), self._stack_names.copy(), _v_))

    def commit(self):
        self._stack.pop()

    def rollback(self):
        self._list_names, self._stack_names, self._dictionary_names = self._stack.pop()


_METHOD_TUPLES = dict(
    _add=[("__add__", None), ("__radd__", (1, 0))],
    _and=[("__and__", None), ("__rand__", (1, 0))],
    _approximate_equal=[("approximate_equal", None)],
    _collection_add=[("add", None)],
    _eq=[("__eq__", None)],
    _ge=[("__ge__", None)],
    _gt=[("__gt__", None)],
    _invert=[("__invert__", None)],
    _le=[("__le__", None)],
    _logical_and=[("and_", None)],
    _logical_not=[("__invert__", None)],
    _logical_or=[("or_", None)],
    _lshift=[("__lshift__", None), ("__rlshift__", (1, 0))],
    _lt=[("__lt__", None)],
    _mod=[("__mod__", None), ("__rmod__", (1, 0))],
    _mul=[("__mul__", None), ("__rmul__", (1, 0))],
    _ne=[("__ne__", None)],
    _or=[("__or__", None), ("__ror__", (1, 0))],
    _pow=[("__pow__", None), ("__rpow__", (1, 0))],
    _rshift=[("__rshift__", None), ("__rrshift__", (1, 0))],
    _shift_right=[("shift_right", None)],
    _sub=[("__sub__", None), ("__rsub__", (1, 0))],
    _truediv=[("__truediv__", None), ("__rtruediv__", (1, 0))],
    _xor=[("__xor__", None), ("__rxor__", (1, 0))],
    APPEND=[("update", None)],
    CONTAINS=[("contains", None)],
    CONTAINSKEY=[("contains", None)],
    GET=[("__getitem__", None)],
    PUT=[("set_item", (0, 2, 1))],
    REMOVE=[("delete_item", None)],
    SET=[("set_item", (0, 2, 1))],
    SIZE=[("len", None)],
    TOBOOLEAN=[("bool", None)],
    TOLONG=[("int", None)],
    TODOUBLE=[("float", None)],
    TOSTRING=[("str", None)],
)

_METHOD_ARGUMENT_TUPLES = dict(
    __add__=(1, set()),
    __and__=(1, set()),
    __eq__=(1, set()),
    __ge__=(1, set()),
    __getitem__=(1, set()),
    __gt__=(1, set()),
    __invert__=(0, set()),
    __le__=(1, set()),
    __lshift__=(1, set()),
    __lt__=(1, set()),
    __mod__=(1, set()),
    __mul__=(1, set()),
    __ne__=(1, set()),
    __or__=(1, set()),
    __pow__=(1, set()),
    __radd__=(1, set()),
    __rand__=(1, set()),
    __rlshift__=(1, set()),
    __rmod__=(1, set()),
    __rmul__=(1, set()),
    __ror__=(1, set()),
    __rpow__=(1, set()),
    __rrshift__=(1, set()),
    __rshift__=(1, set()),
    __rsub__=(1, set()),
    __rtruediv__=(1, set()),
    __rxor__=(1, set()),
    __sub__=(1, set()),
    __truediv__=(1, set()),
    __xor__=(1, set()),
    add=(1, set()),
    and_=(1, set()),
    bool=(0, set()),
    contains=(1, set()),
    delete_item=(1, set()),
    float=(0, set()),
    int=(0, set()),
    len=(0, set()),
    or_=(1, set()),
    set_item=(2, set()),
    str=(0, set()),
    update=(1, set()),
)

_CLASS_BASE_TYPE_NAMES = {"_LONG": "_NUMBER", "_DOUBLE": "_NUMBER"}


def _get_strings():
    function_names = set()
    composite_types = _CompositeTypes()

    _v_ = (
        _Function(function_dictionary, function_names, composite_types)
        for function_dictionary in _get_function_dictionaries().values()
    )
    _v_ = filter(
        skip(lambda function: function.get_code_string(), false_function=True), _v_
    )
    functions = sorted(list(_v_), key=lambda function: function.get_name())

    all_names = ["Macro", "Plus", "Star", "Vlist", "c"]
    for function in functions:
        all_names.extend([function.get_name(), f"{function.get_name()}_stack"])

    yield f"""
# generated by generate_code.py

__all__ = {repr(tuple(all_names))}

import warp10
import typing

N = None
"""[1:-1]

    # prepare type methods
    method_tuples = {}

    for function in functions:
        function_name = function.get_name()

        tuples = _METHOD_TUPLES.get(function_name)
        if tuples is None:
            continue

        for method_name, args_indices in tuples:
            _v_ = _METHOD_ARGUMENT_TUPLES.get(method_name, (None, None))
            args_length, kwargs_names = _v_

            for overload in function.get_overloads():
                args_arguments, kwargs_arguments = overload.get_arguments_tuple()

                if args_length is not None and len(args_arguments) - 1 != args_length:
                    continue

                if kwargs_names is not None and kwargs_arguments.keys() != kwargs_names:
                    continue

                _v_ = 0 if args_indices is None else args_indices.index(0)
                _v_ = args_arguments[_v_].string_index + 1
                _v_ = _get_type_string(overload.dictionary["strings"][_v_], False)
                type_string = _v_

                if type_string == "_LIST" and method_name == "add":
                    name = "append"

                elif type_string == "_LIST" and method_name == "update":
                    name = "extend"

                else:
                    name = method_name

                _v_ = method_tuples.setdefault(type_string, {}).setdefault(name, [])
                _v_.append((overload, args_indices, function_name))

    # types
    for type_name in [
        "ANY",
        "NUMBER",
        "AGGREGATOR",
        "BIGDECIMAL",
        "BITSET",
        "BOOLEAN",
        "BYTES",
        "CELL",
        "CONTEXT",
        "COUNTER",
        "DOUBLE",
        "ENCODER",
        "FILLER",
        "FILTER",
        "FUNC",
        "FUNCTION",
        "GEOSHAPE",
        "GTS",
        "GTSENCODER",
        "KEY",
        "KEYRING",
        "LIST",
        "LONG",
        "MACRO",
        "MAP",
        "MARK",
        "MATCHER",
        "MATRIX",
        "NULL",
        "OBJECT",
        "OPERATOR",
        "PFONT",
        "PGRAPHICS",
        "PIMAGE",
        "PSHAPE",
        "QUATERNION",
        "SET",
        "STRING",
        "TYPE",
        "VECTOR",
        "VLIST",
    ]:
        type_name = f"_{type_name}"

        if type_name == "_ANY":
            typevar_string = ""
            base_string = ""

        elif type_name in ("_LIST", "_SET"):
            typevar_string = f"""
{type_name}_T = typing.TypeVar("{type_name}_T", bound=_ANY)

"""[1:-1]
            base_string = f"(_ANY, typing.Generic[{type_name}_T])"

        elif type_name == "_MAP":
            typevar_string = """
_MAP_KT = typing.TypeVar("_MAP_KT", bound=_ANY)
_MAP_VT = typing.TypeVar("_MAP_VT", bound=_ANY)

"""[1:-1]
            base_string = "(_ANY, typing.Generic[_MAP_KT, _MAP_VT])"

        else:
            typevar_string = ""
            string = _CLASS_BASE_TYPE_NAMES.get(type_name, "_ANY")
            base_string = f"({string})"

        method_strings = {}

        for method_name, tuples in sorted(method_tuples.get(type_name, {}).items()):
            if len(tuples) == 1:
                ((overload, args_indices, function_name),) = tuples
                args_arguments, kwargs_arguments = overload.get_arguments_tuple()

                _v_ = [argument.get_python_name() for argument in args_arguments]
                argument_strings = _v_

                argument_strings[
                    0 if args_indices is None else args_indices.index(0)
                ] = "self"

                _v_ = (f"{name}={name}" for name in sorted(kwargs_arguments))
                argument_strings.extend(_v_)

                arguments_string = ", ".join(argument_strings)
                method_string = f"""
{overload.get_code_string(method_name=method_name, args_indices=args_indices)}
    return {function_name}({arguments_string})
"""[1:-1]

            else:
                _v_ = sorted(tuples, key=lambda tuple: tuple[0].get_sort_key())
                overloads_string = "\n".join(f"""
@typing.overload
{overload.get_code_string(method_name=method_name, args_indices=args_indices)}
    ...
"""[1:-1] for overload, args_indices, _ in _v_)

                assert all_equal(args_indices for _, args_indices, _ in tuples)
                _, args_indices, function_name = tuples[0]
                if args_indices is None:
                    args_string = "self, *args"

                else:
                    args_string = ", ".join(
                        "self" if index == 0 else f"args[{index - 1}]"
                        for index in args_indices
                    )
                method_string = f"""
{overloads_string}
def {method_name}(self, *args, **kwargs):
    return typing.cast(typing.Any, {function_name}({args_string}, **kwargs))
"""[1:-1]

            assert method_name not in method_strings
            method_strings[method_name] = method_string

        _v_ = [method_string for _, method_string in sorted(method_strings.items())]
        body_strings = _v_

        body_strings.append(f"""
{type_name} = None
"""[1:-1])
        body_string = "\n".join(body_strings).replace("\n", "\n    ")
        yield f"""
{typevar_string}class {type_name}{base_string}:
    {body_string}
"""[1:-1]

    # composite types
    yield """
class _stack:
    pass
"""[1:-1]

    for _, name in composite_types._stack_names.items():
        yield f"""
class {name}(_stack):
    {name} = None
"""[1:-1]

    for tuples, name in composite_types._list_names.items():
        properties_string = "\n".join(f"""
    @property
    def {name}(self) -> {type_string}:
        return c(GET(self, {index}), type={type_string})
"""[1:-1] for index, (name, type_string) in enumerate(tuples))

        yield f"""
class {name}(_LIST[_ANY]):
{properties_string}
    {name} = None
"""[1:-1]

    for tuples, name in composite_types._dictionary_names.items():
        properties_string = "\n".join(f"""
    @property
    def {name}(self) -> {type_string}:
        return c(GET(self, {repr(name)}), type={type_string})
"""[1:-1] for name, type_string in tuples)

        yield f"""
class {name}(_MAP[_STRING, _ANY]):
{properties_string}
    {name} = None
"""[1:-1]
    #

    yield f'''
import collections.abc as c_abc
import datetime
import numbers

_Boolean: typing.TypeAlias = bool | _BOOLEAN
_Long: typing.TypeAlias = numbers.Integral | int | datetime.timedelta | _LONG
_Double: typing.TypeAlias = numbers.Real | float | _DOUBLE
_Number: typing.TypeAlias = _Double | _Long | _NUMBER
_String: typing.TypeAlias = str | datetime.datetime | _STRING
_List_PT = typing.TypeVar("_List_PT", bound="_Any")
_List_WT = typing.TypeVar("_List_WT", bound=_ANY)
_List: typing.TypeAlias = c_abc.Sequence[_List_PT] | _LIST[_List_WT]
_Set_PT = typing.TypeVar("_Set_PT", bound="_Any")
_Set_WT = typing.TypeVar("_Set_WT", bound=_ANY)
_Set: typing.TypeAlias = c_abc.Set[_Set_PT] | _SET[_Set_WT]
_Map_PKT = typing.TypeVar("_Map_PKT", bound="_Any")
_Map_PVT = typing.TypeVar("_Map_PVT", bound="_Any")
_Map_WKT = typing.TypeVar("_Map_WKT", bound=_ANY)
_Map_WVT = typing.TypeVar("_Map_WVT", bound=_ANY)
_Map: typing.TypeAlias = c_abc.Mapping[_Map_PKT, _Map_PVT] | _MAP[_Map_WKT, _Map_WVT]
_Any: typing.TypeAlias = (
    _Boolean
    | _Number
    | _String
    | _List["_Any", _ANY]
    | _Set["_Any", _ANY]
    | _Map["_Any", "_Any", _ANY, _ANY]
    | _ANY
)

_Macro_T = typing.TypeVar("_Macro_T", bound=typing.Union[_Any, _stack, None, "Star", "Plus"])
class Macro(tuple[_Macro_T, ...], _MACRO):
    """
    Use `Macro([...])` to create a macro (`<% ... %>`).
    """
    def __repr__(self):
        string = ", ".join(repr(object) for object in self)
        return f"Macro([{{string}}])"

_Plus_T = typing.TypeVar("_Plus_T", bound=typing.Union[_Any, _stack, None, "Star", "Plus"])
class Plus(tuple[_Plus_T, ...]):
    """
    Use `Plus([...])` to create a sequence of (nested) function calls pushing
    one or more objects onto the stack.
    """
    def __new__(cls, *args):
        object = super().__new__(cls, *args)
        assert 1 <= len(object)
        return object

    def __repr__(self):
        string = ", ".join(repr(object) for object in self)
        return f"Plus([{{string}}])"

_Star_T = typing.TypeVar("_Star_T", bound=typing.Union[_Any, _stack, None, "Star", "Plus"])
class Star(tuple[_Star_T, ...]):
    """
    Use `Star([...])` to create a sequence of (nested) function calls pushing
    zero or more objects onto the stack.
    """
    def __repr__(self):
        string = ", ".join(repr(object) for object in self)
        return f"Star([{{string}}])"

_Vlist_T = typing.TypeVar("_Vlist_T", bound=typing.Union[_Any, _stack, None, "Star", "Plus"])
class Vlist(tuple[_Vlist_T, ...], _VLIST):
    """
    Use `Vlist([...])` to create a Vector (`[[ ... ]]`).
    """
    def __repr__(self):
        string = ", ".join(repr(object) for object in self)
        return f"Vlist([{{string}}])"

@typing.overload
def c(object: bool) -> _BOOLEAN:
    """
    Converts a Python bool into a Warp 10 BOOLEAN.
    """
    ...
@typing.overload
def c(object: numbers.Integral | int | datetime.timedelta) -> _LONG:
    """
    Converts a Python integral (e.g. int) or timedelta into a Warp 10 LONG.
    """
    ...
@typing.overload
def c(object: numbers.Real | float) -> _DOUBLE:
    """
    Converts a Python real (e.g. float) into a Warp 10 DOUBLE.
    """
    ...
@typing.overload
def c(object: str | datetime.datetime) -> _STRING:
    """
    Converts a Python string or datetime into a Warp 10 STRING.
    """
    ...
@typing.overload
def c(object: c_abc.Sequence[_Any]) -> _LIST[_ANY]:
    """
    Converts a Python sequence (e.g. list) into a Warp 10 LIST.
    """
    ...
@typing.overload
def c(object: c_abc.Set[_Any]) -> _SET[_ANY]:
    """
    Converts a Python set (e.g. set) into a Warp 10 SET.
    """
    ...
@typing.overload
def c(object: c_abc.Mapping[_Any, _Any]) -> _MAP[_ANY, _ANY]:
    """
    Converts a Python mapping (e.g. dict) into a Warp 10 MAP.
    """
    ...
@typing.overload
def c(object, *, type: type):
    """
    Replaces the type of a Literal.
    """
    ...
def c(object, type=None):
    match object:
        case bool():
            type = _BOOLEAN

        case numbers.Integral() | datetime.timedelta():
            type = _LONG

        case numbers.Real():
            type = _DOUBLE

        case str() | datetime.datetime():
            type = _STRING

        case c_abc.Sequence():
            type = _LIST[_ANY]

        case c_abc.Set():
            type = _SET[_ANY]

        case c_abc.Mapping():
            type = _MAP[_ANY, _ANY]

        case warp10.Literal():
            return typing.cast(typing.Any, warp10.Literal(object.string, type=type))

    return typing.cast(typing.Any, warp10.Literal(warp10.get_string(object), type=type))
'''[1:-1]

    for function in functions:
        yield function.get_code_string()


for string in _get_strings():
    print(string)
    print()
