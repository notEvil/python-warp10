**!! Warning: this is currently in Beta and not ready for production yet !!**

[[_TOC_]]

# Python client for [*Warp 10*](https://github.com/senx/warp10-platform)

The main contribution of this project is a code generator which scrapes [](https://warp10.io/doc/reference) and transforms the WarpScript functions into Python functions and methods with
- type hints,
- docstrings and
- an implementation which generates WarpScript code.

It also provides Python functions to
- generate UUIDs and tokens (`uuid.uuid1`, `warp10.sh tokengen`),
- ingest data (`/api/v0/update`),
- fetch data (`/api/v0/fetch`),
- delete data (`/api/v0/delete`) and
- execute WarpScript code (`/api/v0/exec`)

and build files for an Arch Linux package.

See [Examples](#examples) and [Motivation](#motivation).

## Features

- All WarpScript functions available as Python functions with type hints and docstrings
- Many functions available as (dunder) methods
- Strict type checks at runtime
- Ingest/fetch data as Python objects
- Support for [*Apache Arrow*](https://github.com/apache/arrow) and [*pandas*](https://github.com/pandas-dev/pandas)
  - Efficient ingest/fetch using [*Cython*](https://github.com/cython/cython)
- Converts JSON representation of `GTS` to Python objects
- Commandline interface for generating UUIDs and tokens
- Consistent use of timezone aware datetimes (UTC)

## Examples

![videos/short_example.1.m4v](videos/short_example.1.m4v)
![videos/short_example.2.m4v](videos/short_example.2.m4v)

Remarks
- recorded before function descriptions were added to docstrings
- Editor: [*Neovim*](https://github.com/neovim/neovim)
- Static analysis: [*Pyright*](https://github.com/microsoft/pyright)
- *Neovim* plugin[^f1]: [*coc.nvim*](https://github.com/neoclide/coc.nvim)

## Quickstart

- Create virtual environment (optional but recommended)
  - e.g. install [*Pipenv*](https://github.com/pypa/pipenv) with `python -m pip install pipenv`
- Install Python package
  - e.g. `python -m pipenv run pip install -e .[all]`
- Run tests
  - e.g. `python -m pipenv run python -m pytest ./tests`
- Generate UUIDs and tokens
  - e.g. `python -m pipenv run python -m warp10 create-uuid`
     and `python -m pipenv run python -m warp10 create-token --help`
- Import and use `warp10` and `warp10.warpscript`

## Documentation

**!! Warning:** naive `datetime` objects are **assumed UTC !!** unlike the standard library *datetime* which assumes local time.
You might not like this decision, but trust me, I've been there and its better this way[^f2] :)

### warp10.warpscript

If you are familiar with WarpScript, the majority of this package needs no introduction.
There are however certain differences to WarpScript and ordinary Python packages you might need to know.

#### WarpScript functions

All WarpScript functions are available as Python functions.
- Operators are named `_{name}`, e.g. `!` is named `_logical_not`, and provided as dunder methods of types if possible, see [Warpscript types](#warpscript-types)
- `{name}->` are named `FROM_{name}`
- `->{name}` are named `TO_{name}`
- Special characters are replaced by `_`

All functions are available as "stack" variants named `{name}_stack`.
- Stack variants enable complex use cases
- Stack variants take any number of positional arguments and assume the stack to be in the correct state
- Stack variants require the return type as keyword argument if it is not unique
  - You need to use `typing.cast` to get type hints back

#### WarpScript types

All WarpScript types are available as Python classes named `_{name}`, e.g. `_BOOLEAN`.
- All functions accept objects of the corresponding Python types, e.g. `bool` where `BOOLEAN` is expected
- Many functions are provided as (dunder) methods, e.g. `+` as `__add__` and `__radd__`, and `APPEND` as `append` or `update`
  - if necessary, use `c(Python object)` to use those methods
- Some functions are provided as regular methods instead of dunder methods, e.g. `SIZE` as `len` instead of `__len__`, because Python enforces their return type
- Types are used exclusively for type hints, type checks and dynamic dispatch (they are never instantiated)
  - type hints: used by your IDE to suggest operations and warn about type issues
  - type checks: at runtime, arguments are matched against function signatures
  - dynamic dispatch: methods map to functions depending on return type

There are a couple of custom types.
- `Vlist`: use to create a Vector (`[[ ... ]]`)
- `Macro`: use to create a macro (`<% ... %>`)
- `Star`: use to push zero or more objects onto the stack, e.g. for `CLEAR`
- `Plus`: use to push one or more objects onto the stack, e.g. for `APPLY`
- `_list_{number}`: used to provide access to `LIST` elements by attribute
- `_dict_{number}`: used to provide access to `MAP` items by attribute
- `_stack_{number}`: used to signal multiple objects and suggest the use of stack functions

## Motivation

I discovered *Warp 10* a few years ago and always wanted to use it, but never did.
Mostly because there is a certain complexity barrier that makes it unsuitable for prototyping compared to e.g. [*SQLite*](https://www.sqlite.org).
There are no official packages for common Linux distributions and Docker is Docker.
Also, the official recommendation for interacting with *Warp 10* in Python is [*Py4J*](https://github.com/py4j/py4j) which is far from easy to set up and use.

Now, with more experience with packaging for Arch Linux, I tried to lower the barrier by writing Python functions and types for interacting with the main API endpoints.
This worked out pretty well and I wondered if there was a way to generate WarpScript code in Python, with type checking, code completion and in-editor documentation.
There was no project providing this, so I tried my own luck.

## TODO

- documentation

## mdformat_mytables

In absence of rich Markdown rendering, using Markdown code directly is a useful fallback option provided it is readable.
Unfortunately, table representations as provided by many supersets of Markdown fall short in this regard.
Either cell contents are forced to a single line or forced to be text only ([Pandoc multiline_tables](https://pandoc.org/MANUAL.html#extension-multiline_tables) and [Pandoc grid_tables](https://pandoc.org/MANUAL.html#extension-grid_tables) produce incorrect Markdown code when inline blocks span multiple lines).

[mdformat_mytables.py](mdformat_mytables.py) implements a table representation that splits cell contents such that inline blocks are closed and reopened when needed. Beware! This is a one-way transformation and potentially not reversible.

## Footnotes

[^f1]: also visible: [*VGit*](https://github.com/tanvirtin/vgit.nvim)
[^f2]: if you think otherwise, open an issue and convice me
