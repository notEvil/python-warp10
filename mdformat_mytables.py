import mdformat
import mdformat.plugins as m_plugins
import functools
import itertools
import math
import re


class Object:
    def __init__(self, **kwargs):
        super().__init__()

        for name, object in kwargs.items():
            setattr(self, name, object)


def _render_table(tree_node, render_context, max_width=75):
    # create cells
    child_nodes = _get_child_nodes(tree_node)

    head_cells = []
    thead_nodes = child_nodes.get("thead")
    if thead_nodes is not None:
        (thead_node,) = thead_nodes
        head_cells = [
            [
                _Cell(th_node, render_context)
                for th_node in (_get_child_nodes(tr_node).get("th", []))
            ]
            for tr_node in _get_child_nodes(thead_node).get("tr", [])
        ]

    body_cells = []
    tbody_nodes = child_nodes.get("tbody")
    if tbody_nodes is not None:
        (tbody_node,) = tbody_nodes
        body_cells = [
            [
                _Cell(td_node, render_context)
                for td_node in _get_child_nodes(tr_node).get("td", [])
            ]
            for tr_node in _get_child_nodes(tbody_node).get("tr", [])
        ]

    # create column objects
    column_objects = [
        Object(
            min_width=max(cell._min_width for cell in cells),
            max_width=max(cell._max_width for cell in cells),
        )
        for cells in (itertools.zip_longest(*itertools.chain(head_cells, body_cells)))
    ]

    for column_object in column_objects:  # height / width <= 2 (soft limit)
        _v_ = math.ceil(math.sqrt(column_object.max_width / 2))
        column_object.min_width = max(column_object.min_width, _v_)

    # distribute space
    remaining_space = max_width - (len(column_objects) * 3 + 1)
    remaining_count = len(column_objects)

    for column_object in sorted(column_objects, key=lambda object: object.max_width):
        column_object.width = (
            column_object.max_width
            if column_object.max_width <= remaining_space / remaining_count
            else max(
                column_object.min_width, math.ceil(remaining_space / remaining_count)
            )
        )
        remaining_space -= column_object.width
        # assert not (remaining_space < 0)  # TODO
        remaining_count -= 1

    # create row seperator
    head_strings = [list(string) for string in """
┏━┳┓
┃ ┃┃
┣━╋┫
┡━╇┩
""".strip().split("\n")]
    body_strings = [list(string) for string in """
┌─┬┐
│ ││
├─┼┤
└─┴┘
""".strip().split("\n")]
    separator_strings = body_strings[2]

    parts = [separator_strings[0]]
    for column_object in column_objects:
        _v_ = [separator_strings[1] * (column_object.width + 2), separator_strings[2]]
        parts.extend(_v_)

    if len(parts) != 1:
        parts[-1] = separator_strings[-1]

    row_seperator_string = "".join(parts)
    #

    _v_ = (body_strings if len(head_cells) == 0 else head_strings)[0]
    _v_ = [_get_row_separator_string(row_seperator_string, separator_strings, _v_)]
    line_strings = _v_

    if len(head_cells) != 0:
        for cells in head_cells:
            line_strings.extend(_render_row(cells, column_objects, head_strings[1][0]))

            _v_ = head_strings[2]
            line_strings.append(
                _get_row_separator_string(row_seperator_string, separator_strings, _v_)
            )

        line_strings[-1] = _get_row_separator_string(
            row_seperator_string, separator_strings, head_strings[3]
        )

    if len(body_cells) != 0:
        for cells in body_cells:
            line_strings.extend(_render_row(cells, column_objects, body_strings[1][0]))
            line_strings.append(row_seperator_string)

        line_strings[-1] = _get_row_separator_string(
            row_seperator_string, separator_strings, body_strings[3]
        )

    return "\n".join(line_strings)


def _get_child_nodes(tree_node):
    tree_nodes = {}
    for child_node in tree_node.children:
        tree_nodes.setdefault(child_node.type, []).append(child_node)

    return tree_nodes


class _Cell:
    def __init__(self, tree_node, render_context):
        super().__init__()

        self.tree_node = tree_node
        self.render_context = render_context

        # initialize
        self._min_width = 0
        self._max_width = 0
        self._objects = {}

        # render
        self._column_width = None
        self._remaining_space = None
        self._strings = None
        self._object_stack = None

        self._initialize(tree_node)

    def _initialize(self, tree_node):
        match tree_node.type:
            case "text":
                _v_ = self._get_atom_matches(tree_node.content)
                _v_ = max((len(match.group(0)) for match in _v_), default=1)
                self._min_width = max(self._min_width, _v_)

                self._max_width += len(tree_node.content)

            case "code_inline":
                _v_ = re.finditer(r"`+", tree_node.content)
                _v_ = "`" * (max((len(match.group(0)) for match in _v_), default=0) + 1)
                string = _v_

                _v_ = Object(prefix_string=string, suffix_string=string)
                self._objects[tree_node] = _v_

                _v_ = self._get_atom_matches(tree_node.content)
                _v_ = max((len(match.group(0)) for match in _v_), default=1)
                _v_ = len(string) + (len(string) != 1) + _v_ + (len(string) != 1)
                self._min_width = max(self._min_width, _v_ + len(string))

                _v_ = len(string) + tree_node.content.startswith("`")
                _v_ = _v_ + len(tree_node.content) + tree_node.content.endswith("`")
                self._max_width += _v_ + len(string)

            case "em":
                self._objects[tree_node] = Object(prefix_string="*", suffix_string="*")
                self._min_width = max(self._min_width, 3)
                self._max_width += 2

            case "strong":
                _v_ = Object(prefix_string="**", suffix_string="**")
                self._objects[tree_node] = _v_

                self._min_width = max(self._min_width, 5)
                self._max_width += 4

            case "link":
                object = Object(string=tree_node.render(self.render_context))
                self._objects[tree_node] = object
                self._min_width = max(self._min_width, len(object.string))
                self._max_width += len(object.string)

            case "th" | "td" | "inline":
                pass

            case _:
                raise Exception(f"{repr(tree_node.type)}")

        for tree_node in tree_node.children:
            self._initialize(tree_node)

    def render(self, width):
        self._column_width = width
        self._remaining_space = width
        self._strings = []
        self._object_stack = []

        _v_ = self.tree_node.attrs.get("style", "")
        match = re.search(r"text-align:(?P<align>\w+)", _v_)

        if match is None:
            justify = lambda string: string.ljust(width)

        else:
            match match.group("align"):
                case "left":
                    justify = lambda string: string.ljust(width)

                case "center":
                    justify = lambda string: string.center(width)

                case "right":
                    justify = lambda string: string.rjust(width)

                case _:
                    raise ValueError(match.group("align"))

        for line_string in self._render(self.tree_node):
            yield justify(line_string)

        for line_string in self._finalize():
            yield justify(line_string)

    def _render(self, tree_node):
        is_code_inline = tree_node.type == "code_inline"
        object = None

        # prefix
        match tree_node.type:
            case "code_inline" | "em" | "strong":
                object = self._objects[tree_node]

                _v_ = len(object.prefix_string) + 1 + len(object.suffix_string)
                if self._remaining_space < _v_:  # not enough space
                    yield from self._finalize()

                    self._remaining_space = self._column_width - sum(
                        len(object.prefix_string) + len(object.suffix_string)
                        for object in self._object_stack
                    )

                    _v_ = [object.prefix_string for object in self._object_stack]
                    self._strings = _v_

                _v_ = len(object.prefix_string) + len(object.suffix_string)
                self._remaining_space -= _v_

                self._strings.append(object.prefix_string)
                self._object_stack.append(object)

        # content
        match tree_node.type:
            case "text" | "code_inline":
                string = tree_node.content
                remaining_space = self._get_remaining_space(string, is_code_inline)

                while remaining_space < len(string):  # not enough space
                    # prepare split
                    atom_slices = _Slices()
                    for match in self._get_atom_matches(string):
                        if remaining_space <= match.start():
                            break

                        atom_slices.add(match.start(), match.end())

                    # try split after space
                    index = remaining_space
                    while True:
                        index = string.rfind(" ", 0, index)
                        if index == -1:  # no space
                            index = atom_slices.get_index(remaining_space)
                            break

                        if atom_slices.get_index(index + 1) == index + 1:
                            index += 1
                            break
                    #

                    if index != 0:
                        if is_code_inline and string.startswith("`"):  # separate `
                            self._strings.append(" ")

                        self._strings.append(string[:index])

                    yield from self._finalize()

                    self._remaining_space = self._column_width - sum(
                        len(object.prefix_string) + len(object.suffix_string)
                        for object in self._object_stack
                    )

                    _v_ = [object.prefix_string for object in self._object_stack]
                    self._strings = _v_

                    string = string[index:]
                    remaining_space = self._get_remaining_space(string, is_code_inline)

                if is_code_inline and string.startswith("`"):  # separate `
                    string = f" {string}"

                self._remaining_space -= len(string)
                self._strings.append(string)

            case "link":
                object = self._objects[tree_node]
                if self._remaining_space < len(object.string):
                    yield from self._finalize()

                    self._remaining_space = self._column_width - sum(
                        len(object.prefix_string) + len(object.suffix_string)
                        for object in self._object_stack
                    )

                    _v_ = [object.prefix_string for object in self._object_stack]
                    self._strings = _v_

                self._remaining_space -= len(object.string)
                self._strings.append(object.string)

            case "th" | "td" | "inline" | "em" | "strong":
                pass

            case _:
                raise Exception(f"{repr(tree_node.type)}")

        if not (object is not None and hasattr(object, "string")):
            for child_node in tree_node.children:
                yield from self._render(child_node)

        # suffix
        match tree_node.type:
            case "code_inline" | "em" | "strong":
                self._strings.append(self._object_stack.pop().suffix_string)

    def _get_remaining_space(self, string, is_code_inline):
        remaining_space = self._remaining_space

        if is_code_inline:
            if string.startswith("`"):  # prefix and ` will be separated
                remaining_space -= 1

            _v_ = string[remaining_space - 1 : remaining_space] == "`"
            if _v_:  # ` and suffix will be separated  # TODO test
                remaining_space -= 1

        return remaining_space

    def _finalize(self):
        # remove unused prefixes
        objects = []
        while True:
            _v_ = len(self._object_stack) != 0
            if not (_v_ and self._strings[-1] == self._object_stack[-1].prefix_string):
                break

            self._strings.pop()
            objects.append(self._object_stack.pop())
        #

        _v_ = len(self._strings) != 0 and self._strings[-1].endswith("`")
        _v_ = _v_ and len(self._object_stack) != 0
        if _v_ and self._object_stack[-1].suffix_string.startswith("`"):  # separate `
            self._strings.append(" ")

        _v_ = (object.suffix_string for object in self._object_stack[::-1])
        self._strings.extend(_v_)

        string = "".join(self._strings)
        if len(self._object_stack) == 0:
            string = string.strip()

        if string != "":
            yield string

        self._object_stack.extend(objects[::-1])

    def _get_atom_matches(self, string):
        return re.finditer(
            r"(?<![\w-])-?\d+(\.\d*)?([eE]-?\d+)?(?![\w.])|[a-zA-Z_]\w+[^\w\s]?|.[^\w\s]",
            string,
        )


class _Slices:
    def __init__(self):
        super().__init__()

        self._indices = []

    def add(self, start_index, stop_index):
        self._indices.append((start_index, stop_index))

    def get_index(self, index):
        for start_index, stop_index in self._indices:
            if start_index < index and index < stop_index:
                return start_index

        return index


def _get_row_separator_string(row_seperator_string, strings, with_strings):
    for index, string in enumerate(strings):
        row_seperator_string = row_seperator_string.replace(string, with_strings[index])

    return row_seperator_string


def _render_row(cells, column_objects, seperator_character):
    _v_ = itertools.zip_longest(cells, column_objects)
    _v_ = (cell.render(column_object.width) for cell, column_object in _v_)
    for strings in itertools.zip_longest(*_v_):
        string = f" {seperator_character} ".join(
            " " * column_object.width if string is None else string
            for string, column_object in itertools.zip_longest(strings, column_objects)
        )
        yield f"{seperator_character} {string} {seperator_character}"


RENDERERS = {"table": _render_table}


def update_mdit(markdown_it):
    pass


def convert_tables(string, width=75):
    if "mytables" not in m_plugins.PARSER_EXTENSIONS:
        import mdformat_mytables

        m_plugins.PARSER_EXTENSIONS["mytables"] = mdformat_mytables

    RENDERERS["table"] = functools.partial(_render_table, max_width=width)  # TODO

    _v_ = mdformat.text(string, extensions=["tables"], options=dict(wrap=width))
    string = mdformat.text(_v_, extensions=["mytables", "tables"])

    RENDERERS["table"] = _render_table
    return string
