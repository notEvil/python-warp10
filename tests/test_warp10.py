import warp10
import warp10.warpscript as w_warpscript
import datetime
import inspect
import typing


_LITERALS = {
    w_warpscript._Boolean: [False],
    w_warpscript._Boolean | None: [None, False],
    w_warpscript._Long: [0, datetime.timedelta()],
    w_warpscript._Long | None: [None, 0, datetime.timedelta()],
    w_warpscript._Double: [0.0],
    w_warpscript._Double | None: [None, 0.0],
    w_warpscript._Number: [0, datetime.timedelta(), 0.0],
    w_warpscript._String: [
        "",
        datetime.datetime.min.replace(tzinfo=datetime.timezone.utc),
    ],
    w_warpscript._String
    | None: [None, "", datetime.datetime.min.replace(tzinfo=datetime.timezone.utc)],
    w_warpscript._Map: [{}],
    w_warpscript._Map | None: [None, {}],
    w_warpscript._Any: [False],
    w_warpscript._Any | None: [None, False],
    w_warpscript._AGGREGATOR: [w_warpscript.bucketizer_and()],
    w_warpscript._AGGREGATOR | None: [None, w_warpscript.bucketizer_and()],
    w_warpscript._BIGDECIMAL: [w_warpscript.TO_BD("")],
    w_warpscript._BITSET: [w_warpscript.BYTESTOBITS(w_warpscript.TO_BYTES("", ""))],
    w_warpscript._BYTES: [w_warpscript.TO_BYTES("", "")],
    w_warpscript._BYTES | None: [None, w_warpscript.TO_BYTES("", "")],
    w_warpscript._CONTEXT: [w_warpscript.SAVE()],
    w_warpscript._COUNTER: [w_warpscript.COUNTER()],
    w_warpscript._FILLER: [w_warpscript.filler_interpolate()],
    w_warpscript._FILTER: [w_warpscript.filter_all_eq(False)],
    w_warpscript._FUNCTION: [w_warpscript.FUNCREF("")],
    w_warpscript._GEOSHAPE: [w_warpscript.TO_GEOSHAPE([0])],
    w_warpscript._GTS: [w_warpscript.NEWGTS()],
    w_warpscript._GTSENCODER: [w_warpscript.NEWENCODER()],
    w_warpscript._KEY: [w_warpscript.ECPRIVATE({})],
    w_warpscript._KEY | None: [None, w_warpscript.ECPRIVATE({})],
    w_warpscript._KEYRING: [
        w_warpscript.c(w_warpscript.PGPRING("")[0], type=w_warpscript._KEYRING)
    ],
    w_warpscript._KEYRING
    | None: [
        None,
        w_warpscript.c(w_warpscript.PGPRING("")[0], type=w_warpscript._KEYRING),
    ],
    w_warpscript._MACRO: [w_warpscript.Macro()],
    w_warpscript._MACRO | None: [None, w_warpscript.Macro()],
    w_warpscript._MARK: [w_warpscript.MARK()],
    w_warpscript._MATCHER: [w_warpscript.MATCHER("")],
    w_warpscript._MATRIX: [w_warpscript.TO_MAT([])],
    w_warpscript._OBJECT: [w_warpscript.STDIN()],
    w_warpscript._OBJECT | None: [None, w_warpscript.STDIN()],
    w_warpscript._OPERATOR: [w_warpscript.op_add()],
    w_warpscript._PGRAPHICS: [w_warpscript.PGraphics(0, 0, "")],
    w_warpscript._PIMAGE: [w_warpscript.Pdecode("")],
    w_warpscript._PSHAPE: [w_warpscript.PloadShape("")],
    w_warpscript._VECTOR: [w_warpscript.TO_VEC([])],
    w_warpscript._VLIST: [w_warpscript.Vlist()],
    w_warpscript._List: [[]],
    w_warpscript._List | None: [None, []],
    w_warpscript._List[w_warpscript._Long, w_warpscript._LONG]: [
        [0],
        [datetime.timedelta()],
    ],
    w_warpscript._List[w_warpscript._Double, w_warpscript._DOUBLE]: [[0.0]],
    w_warpscript._List[w_warpscript._Number, w_warpscript._NUMBER]: [
        [0],
        [datetime.timedelta()],
        [0.0],
    ],
    w_warpscript._List[w_warpscript._String, w_warpscript._STRING]: [
        ["", datetime.datetime.min.replace(tzinfo=datetime.timezone.utc)]
    ],
    w_warpscript._List[w_warpscript._String, w_warpscript._STRING]
    | None: [None, ["", datetime.datetime.min.replace(tzinfo=datetime.timezone.utc)]],
    w_warpscript._List[w_warpscript._Map, w_warpscript._MAP]: [[{}]],
    w_warpscript._List[w_warpscript._Any, w_warpscript._ANY]: [[False]],
    w_warpscript._List[w_warpscript._BYTES, w_warpscript._BYTES]: [
        [w_warpscript.TO_BYTES("", "")]
    ],
    w_warpscript._List[w_warpscript._COUNTER, w_warpscript._COUNTER]: [
        [w_warpscript.COUNTER()]
    ],
    w_warpscript._List[w_warpscript._GEOSHAPE, w_warpscript._GEOSHAPE]: [
        [w_warpscript.TO_GEOSHAPE([0])]
    ],
    w_warpscript._List[w_warpscript._GTS, w_warpscript._GTS]: [[w_warpscript.NEWGTS()]],
    w_warpscript._List[w_warpscript._GTS, w_warpscript._GTS]
    | None: [None, [w_warpscript.NEWGTS()]],
    w_warpscript._List[w_warpscript._GTSENCODER, w_warpscript._GTSENCODER]: [
        [w_warpscript.NEWENCODER()]
    ],
    w_warpscript._List[w_warpscript._List, w_warpscript._LIST]: [[[]]],
    w_warpscript._List[
        w_warpscript._List[w_warpscript._Long, w_warpscript._LONG],
        w_warpscript._LIST[w_warpscript._LONG],
    ]: [[[0]]],
    w_warpscript._Set: [set()],
    w_warpscript._Set[w_warpscript._Long, w_warpscript._LONG]: [{0}],
    w_warpscript.Plus[w_warpscript._GTS]: [w_warpscript.Plus([w_warpscript.NEWGTS()])],
    w_warpscript.Plus[w_warpscript._List[w_warpscript._GTS, w_warpscript._GTS]]: [
        w_warpscript.Plus([[w_warpscript.NEWGTS()]])
    ],
    w_warpscript.Star[w_warpscript._String]: [w_warpscript.Star([""])],
    w_warpscript.Star[w_warpscript._Any]: [w_warpscript.Star([False])],
    w_warpscript._CELL: [warp10.Literal("", type=w_warpscript._CELL)],
    w_warpscript._ENCODER: [warp10.Literal("", type=w_warpscript._ENCODER)],
    w_warpscript._PFONT: [warp10.Literal("", type=w_warpscript._PFONT)],
    w_warpscript._List[w_warpscript._ENCODER, w_warpscript._ENCODER]: [
        [warp10.Literal("", type=w_warpscript._ENCODER)]
    ],
}  # col: skip


def test_to_string():
    for object_name in w_warpscript.__all__:
        if object_name in ("Macro", "Plus", "Star", "Vlist", "c"):
            continue

        function_name = object_name
        function = getattr(w_warpscript, function_name)

        overload_functions = typing.get_overloads(function)
        if len(overload_functions) == 0:
            overload_functions = [function]

        for overload_function in overload_functions:
            signature = inspect.signature(overload_function)
            args_lists = []
            kwargs_lists = {}
            for parameter in signature.parameters.values():
                match parameter.kind:
                    case inspect.Parameter.POSITIONAL_ONLY:
                        args_lists.append(_LITERALS[parameter.annotation])

                    case inspect.Parameter.KEYWORD_ONLY:
                        _v_ = function_name.endswith("_stack")
                        if _v_ and parameter.name == "type":
                            kwargs_lists["type"] = [signature.return_annotation]

                        else:
                            _v_ = _LITERALS[parameter.annotation]
                            kwargs_lists[parameter.name] = _v_

                    case inspect.Parameter.VAR_POSITIONAL:
                        args_lists.append([False])

                    case _:
                        raise ValueError(parameter.kind)

            _v_ = any(len(args) == 0 for args in args_lists)
            if _v_ or any(len(kwargs) == 0 for kwargs in kwargs_lists.values()):
                continue

            min_args = [args[0] for args in args_lists]
            min_kwargs = {name: kwargs[0] for name, kwargs in kwargs_lists.items()}

            _v_ = signature.return_annotation
            _test(min_args, min_kwargs, _v_, function, function_name)

            args = list(min_args)
            for index, objects in enumerate(args_lists):
                for object in objects:
                    args[index] = object

                    _v_ = signature.return_annotation
                    _test(args, min_kwargs, _v_, function, function_name)

                args[index] = min_args[index]

            kwargs = dict(min_kwargs)
            for name, objects in kwargs_lists.items():
                for object in objects:
                    kwargs[name] = object

                    _v_ = signature.return_annotation
                    _test(min_args, kwargs, _v_, function, function_name)

                kwargs[name] = min_kwargs[name]

            literal = _test(
                (args[-1] for args in args_lists),
                {name: kwargs[-1] for name, kwargs in kwargs_lists.items()},
                signature.return_annotation,
                function,
                function_name,
            )
            _test_sub(literal)


_RETURN_TYPES = dict(
    ECPUBLIC=[w_warpscript._KEY, w_warpscript._MAP],
    FROM_JSON=[w_warpscript._LIST, w_warpscript._MAP],
    GEO_BUFFER=[None, w_warpscript._BYTES, w_warpscript._STRING],
    WRAP=[w_warpscript._STRING, w_warpscript._LIST[w_warpscript._STRING]],
    WRAPFAST=[w_warpscript._BYTES, w_warpscript._LIST[w_warpscript._BYTES]],
    WRAPMV=[w_warpscript._BYTES, w_warpscript._LIST[w_warpscript._BYTES]],
    WRAPMV_=[w_warpscript._BYTES, w_warpscript._LIST[w_warpscript._BYTES]],
    WRAPOPT=[w_warpscript._STRING, w_warpscript._LIST[w_warpscript._STRING]],
    WRAPRAW=[w_warpscript._BYTES, w_warpscript._LIST[w_warpscript._BYTES]],
    WRAPRAWOPT=[w_warpscript._BYTES, w_warpscript._LIST[w_warpscript._BYTES]],
)  # TODO


def _test(args, kwargs, return_type, function, name):
    literal = function(*args, **kwargs)
    assert literal.type in _RETURN_TYPES.get(name, [return_type])
    str(literal)
    return literal


def _test_sub(literal):
    if literal.type is None or not (
        literal.type.__name__.startswith("_list_")
        or literal.type.__name__.startswith("_dict_")
    ):
        return

    for name, object in vars(literal.type).items():
        if not isinstance(object, property):
            continue

        sub_literal = getattr(literal, name)
        assert sub_literal.type == inspect.signature(object.fget).return_annotation
        str(sub_literal)

        _test_sub(sub_literal)


def test__get_type():
    assert warp10._get_type(False) == w_warpscript._BOOLEAN
    assert warp10._get_type(0) == w_warpscript._LONG
    assert warp10._get_type(datetime.timedelta()) == w_warpscript._LONG
    assert warp10._get_type(0.0) == w_warpscript._DOUBLE
    assert warp10._get_type("") == w_warpscript._STRING
    assert warp10._get_type(datetime.datetime.min) == w_warpscript._STRING
    # list
    assert warp10._get_type([]) == w_warpscript._LIST
    assert warp10._get_type([False]) == w_warpscript._LIST[w_warpscript._BOOLEAN]
    assert warp10._get_type([False, 1]) == w_warpscript._LIST[w_warpscript._ANY]
    assert warp10._get_type([0, 1.0]) == w_warpscript._LIST[w_warpscript._NUMBER]
    # set
    assert warp10._get_type(set()) == w_warpscript._SET
    assert warp10._get_type({False}) == w_warpscript._SET[w_warpscript._BOOLEAN]
    assert warp10._get_type({False, 1}) == w_warpscript._SET[w_warpscript._ANY]
    assert warp10._get_type({0, 1.0}) == w_warpscript._SET[w_warpscript._NUMBER]
    # dict
    assert warp10._get_type({}) == w_warpscript._MAP

    _v_ = w_warpscript._MAP[w_warpscript._BOOLEAN, w_warpscript._LONG]
    assert warp10._get_type({False: 0}) == _v_

    _v_ = w_warpscript._MAP[w_warpscript._BOOLEAN, w_warpscript._LONG]
    assert warp10._get_type({False: 0, True: 1}) == _v_

    _v_ = w_warpscript._MAP[w_warpscript._ANY, w_warpscript._BOOLEAN]
    assert warp10._get_type({False: False, 1: False}) == _v_

    _v_ = w_warpscript._MAP[w_warpscript._NUMBER, w_warpscript._BOOLEAN]
    assert warp10._get_type({0: False, 1.0: False}) == _v_

    _v_ = w_warpscript._MAP[w_warpscript._BOOLEAN, w_warpscript._ANY]
    assert warp10._get_type({False: False, True: 1}) == _v_

    _v_ = w_warpscript._MAP[w_warpscript._BOOLEAN, w_warpscript._NUMBER]
    assert warp10._get_type({False: 0, True: 1.0}) == _v_

    _v_ = w_warpscript._MAP[w_warpscript._ANY, w_warpscript._ANY]
    assert warp10._get_type({False: False, 1: 1}) == _v_

    _v_ = w_warpscript._MAP[w_warpscript._NUMBER, w_warpscript._NUMBER]
    assert warp10._get_type({0: 0, 1.0: 1.0}) == _v_

    # complex
    _v_ = w_warpscript._LIST[w_warpscript._SET[w_warpscript._ANY]]
    assert warp10._get_type([{False}, {0}]) == _v_


def test__is_subclass():
    # simple inheritance
    assert warp10._is_subclass(w_warpscript._NUMBER, w_warpscript._ANY)
    assert not warp10._is_subclass(w_warpscript._ANY, w_warpscript._NUMBER)
    # union type
    assert warp10._is_subclass(bool, w_warpscript._Boolean)
    assert not warp10._is_subclass(int, w_warpscript._Boolean)
    # generic type
    base_type = w_warpscript._List
    assert warp10._is_subclass(list, base_type)
    assert not warp10._is_subclass(set, base_type)
    assert warp10._is_subclass(list[bool], base_type)
    assert not warp10._is_subclass(set[bool], base_type)
    base_type = w_warpscript._List[w_warpscript._Boolean, w_warpscript._BOOLEAN]
    assert warp10._is_subclass(list, base_type)
    assert not warp10._is_subclass(set, base_type)
    assert warp10._is_subclass(list[bool], base_type)
    assert not warp10._is_subclass(set[bool], base_type)
    assert not warp10._is_subclass(list[int], base_type)
    # complex
    _v_ = w_warpscript._Set[w_warpscript._Boolean, w_warpscript._BOOLEAN]
    base_type = w_warpscript._List[_v_, w_warpscript._SET[w_warpscript._BOOLEAN]]

    assert warp10._is_subclass(list[set[bool]], base_type)
    assert not warp10._is_subclass(list[set[int]], base_type)


def test_get_timestamp():
    _datetime = datetime.datetime.now()
    timestamp = int(_datetime.replace(tzinfo=datetime.timezone.utc).timestamp() * 1e6)

    assert warp10.get_timestamp(_datetime) == timestamp

    _v_ = warp10.get_timestamp(_datetime.replace(tzinfo=datetime.timezone.utc))
    assert _v_ == timestamp

    _v_ = warp10.get_datetime(warp10.get_timestamp(_datetime))
    assert _v_ == _datetime.replace(tzinfo=datetime.timezone.utc)
