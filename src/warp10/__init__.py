import collections.abc as c_abc
import dataclasses
import datetime
import functools
import importlib
import inspect
import json
import numbers
import operator
import re
import types
import typing
import urllib.parse as u_parse


if typing.TYPE_CHECKING:
    import pyarrow


class NONE:
    """
    For instance, use `name = dictionary.get(key, default=NONE); name is NONE`
    instead of `try: name = dictionary[key]; except KeyError: ...`.
    """

    pass


def get_utc(datetime_: datetime.datetime) -> datetime.datetime:
    """
    Converts datetime object to time zone UTC.
    """
    return (
        datetime_.replace(tzinfo=datetime.timezone.utc)
        if datetime_.tzinfo is None
        else datetime_.astimezone(datetime.timezone.utc)
    )


DEFAULT_ADDRESS = "127.0.0.1"
DEFAULT_PORT = 8080
BUFFER_SIZE = 40_000  # (timestamp, float) has length 39

_STRING_PATTERN = r"[\w.%]+"
_ITEM_PATTERN = rf"{_STRING_PATTERN}={_STRING_PATTERN}"

_v_ = rf"(?P<c>{_STRING_PATTERN})\{{(?P<l>{_ITEM_PATTERN}(,{_ITEM_PATTERN})*)?\}}"
_GTS_PATTERN = _v_


class Literal:
    """
    Core type used to track the return type of (nested) function calls or
    sequences of (nested) function calls and dispatch (dunder) methods
    accordingly.

    It is usually not necessary to create `Literal` directly.
    """

    def __init__(self, string: str, type: type | None = None, _bool=None):
        super().__init__()

        self.string = string
        self.type = type
        self._bool = _bool

    def __add__(self, object):
        return self._call("__add__", args=(object,))

    def __and__(self, object):
        return self._call("__and__", args=(object,))

    def __eq__(self, object):
        bool = isinstance(object, Literal) and object.string == self.string
        if self.type is None:
            return Literal(typing.cast(str, _NotAString()), _bool=bool)

        literal = self._call("__eq__", args=(object,))
        literal._bool = bool
        return literal

    def __ge__(self, object):
        return self._call("__ge__", args=(object,))

    def __getattr__(self, name):
        type, _ = _split_type(self.type)
        object = inspect.getattr_static(type, name)

        if inspect.isfunction(object):
            return functools.partial(object, self)

        if isinstance(object, property) and object.fget is not None:
            return object.fget(self)

        return getattr(type, name)

    def __getitem__(self, object):
        return self._call("__getitem__", args=(object,), raise_=True)

    def __gt__(self, object):
        return self._call("__gt__", args=(object,))

    def __invert__(self):
        return self._call("__invert__", raise_=True)

    def __le__(self, object):
        return self._call("__le__", args=(object,))

    def __lshift__(self, object):
        return self._call("__lshift__", args=(object,))

    def __lt__(self, object):
        return self._call("__lt__", args=(object,))

    def __mod__(self, object):
        return self._call("__mod__", args=(object,))

    def __mul__(self, object):
        return self._call("__mul__", args=(object,))

    def __ne__(self, object):
        return self._call("__ne__", args=(object,))

    def __or__(self, object):
        return self._call("__or__", args=(object,))

    def __pow__(self, object):
        return self._call("__pow__", args=(object,))

    def __radd__(self, object):
        return self._call("__radd__", args=(object,))

    def __rand__(self, object):
        return self._call("__rand__", args=(object,))

    def __rlshift__(self, object):
        return self._call("__rlshift__", args=(object,))

    def __rmod__(self, object):
        return self._call("__rmod__", args=(object,))

    def __rmul__(self, object):
        return self._call("__rmul__", args=(object,))

    def __ror__(self, object):
        return self._call("__ror__", args=(object,))

    def __rpow__(self, object):
        return self._call("__rpow__", args=(object,))

    def __rrshift__(self, object):
        return self._call("__rrshift__", args=(object,))

    def __rshift__(self, object):
        return self._call("__rshift__", args=(object,))

    def __rsub__(self, object):
        return self._call("__rsub__", args=(object,))

    def __rtruediv__(self, object):
        return self._call("__rtruediv__", args=(object,))

    def __rxor__(self, object):
        return self._call("__rxor__", args=(object,))

    def __sub__(self, object):
        return self._call("__sub__", args=(object,))

    def __truediv__(self, object):
        return self._call("__truediv__", args=(object,))

    def __xor__(self, object):
        return self._call("__xor__", args=(object,))

    def _call(self, name, args=None, kwargs=None, raise_=False):
        if self.type is None:
            if raise_:
                raise NotImplementedError

            return NotImplemented

        function = getattr(_split_type(self.type)[0], name, None)
        if function is None:
            if raise_:
                raise NotImplementedError

            return NotImplemented

        _v_ = {} if kwargs is None else kwargs
        return function(self, *() if args is None else args, **_v_)

    def __bool__(self):
        return self._bool

    def __hash__(self):
        return hash(self.string)

    def __repr__(self):
        _v_ = repr(self.string)
        return f"Literal({_v_}, type={repr(self.type)}, _bool={repr(self._bool)})"

    def __str__(self):
        return self.string


class _NotAString:
    def __str__(self):
        raise NotImplementedError


def get_string(object: typing.Any, pretty: bool = False) -> str:
    """
    Core function used to convert Python objects to Warp 10 equivalents.

    It is usually not necessary to call `get_string` directly.
    """

    match object:
        case bool():
            return "T" if object else "F"

        case numbers.Integral():
            return str(int(object))

        case datetime.timedelta():
            return str(int(object.total_seconds() * 1_000_000))

        case numbers.Real():
            return str(float(object))

        case str():
            return f"'{u_parse.quote(str(object))}'"

        case datetime.datetime():
            string = get_utc(object).strftime("%Y-%m-%dT%H:%M:%S.%fZ")
            return f"'{string}'"

        case Literal():
            return object.string

        case c_abc.Sequence() | c_abc.Set():
            import warp10.warpscript as w_warpscript

            match object:
                case w_warpscript.Macro():
                    left_string = "<% "
                    right_string = " %>"

                case w_warpscript.Vlist():
                    left_string = "[[ "
                    right_string = " ]]"

                case w_warpscript.Star() | w_warpscript.Plus():
                    left_string = ""
                    right_string = ""

                case c_abc.Sequence():
                    left_string = "[ "
                    right_string = " ]"

                case c_abc.Set():
                    left_string = "( "
                    right_string = " )"

                case _:
                    raise Exception

            if len(object) == 0:
                return f"{left_string.strip()}{right_string.strip()}"

            if pretty:
                _v_ = (
                    get_string(object, pretty=True).replace("\n", "\n  ")
                    for object in object
                )
                string = "\n  ".join(_v_)

                return f"{left_string.strip()}\n  {string}\n{right_string.strip()}"

            string = " ".join(get_string(object) for object in object)
            return f"{left_string}{string}{right_string}"

        case c_abc.Mapping():
            if len(object) == 0:
                return "{}"

            if pretty:
                _v_ = "\n".join(_get_item_string(tuple) for tuple in (object.items()))
                string = _v_.replace("\n", "\n  ")

                return f"{{\n  {string}\n}}"

            string = " ".join(
                f"{get_string(key_object)} {get_string(value_object)}"
                for key_object, value_object in object.items()
            )
            return f"{{ {string} }}"

    raise TypeError(type(object))


def _get_item_string(tuple):
    key_object, value_object = tuple

    key_string = get_string(key_object)
    indent_string = " " * (len(key_string) + 1)

    _v_ = get_string(value_object, pretty=True).replace("\n", f"\n{indent_string}")
    value_string = _v_

    return f"{key_string} {value_string}"


_v_ = datetime.datetime(1970, 1, 1).timestamp()


def get_timestamp(datetime: datetime.datetime, _zero=_v_) -> int:
    """
    Utility function to convert Python datetime to Warp 10 timestamp.
    """
    return int((datetime.timestamp() - (_zero if datetime.tzinfo is None else 0)) * 1e6)


def get_datetime(timestamp: numbers.Integral | int) -> datetime.datetime:
    """
    Utility function to convert Warp 10 timestamp to Python datetime.
    """
    return datetime.datetime.fromtimestamp(timestamp * 1e-6, tz=datetime.timezone.utc)


class PointPosition:
    """
    Part of `DataPoint`.
    """

    __slots__ = ("latitude", "longitude", "elevation")

    def __init__(
        self,
        latitude: numbers.Real | float | None,
        longitude: numbers.Real | float | None,
        elevation: numbers.Integral | int | None,
    ):
        super().__init__()

        assert not ((latitude is None) ^ (longitude is None))

        self.latitude = latitude
        self.longitude = longitude
        self.elevation = elevation

    def get_string(self) -> str:
        elevation_string = "" if self.elevation is None else self.elevation

        if self.latitude is None:
            return f"/{elevation_string}"

        return f"{self.latitude}:{self.longitude}/{elevation_string}"

    def __hash__(self):
        return hash((PointPosition, self.latitude, self.longitude, self.elevation))

    def __eq__(self, object):
        if type(object) != PointPosition:
            return NotImplemented

        _v_ = object.latitude == self.latitude and object.longitude == self.longitude
        return _v_ and object.elevation == self.elevation

    def __repr__(self):
        _v_ = repr(self.longitude)
        return f"PointPosition({repr(self.latitude)}, {_v_}, {repr(self.elevation)})"


class GtsId:
    """
    Identifier of a Geo Time Series (GTS), usually as part of `DataPoint`.
    """

    def __init__(self, class_name: str, labels: c_abc.Mapping[str, str]):
        super().__init__()

        self.class_name = class_name
        self.labels = labels

        self._labels = frozenset(labels.items())

    def get_string(self) -> str:
        labels_string = ",".join(
            f"{u_parse.quote(key_string)}={u_parse.quote(value_string)}"
            for key_string, value_string in self._labels
        )
        return f"{u_parse.quote(self.class_name)}{{{labels_string}}}"

    def __hash__(self):
        return hash((GtsId, self.class_name, self._labels))

    def __eq__(self, object):
        if object is self:
            return True

        if type(object) != GtsId:
            return NotImplemented

        return object.class_name == self.class_name and object._labels == self._labels

    def __repr__(self):
        return f"GtsId({repr(self.class_name)}, {repr(self.labels)})"


class DataPoint:
    """
    A single data point.
    """

    __slots__ = ("datetime", "point_position", "gts_id", "value")

    def __init__(
        self,
        datetime: datetime.datetime | None,
        point_position: PointPosition | None,
        gts_id: GtsId,
        value: bool | numbers.Integral | int | numbers.Real | float | str,
    ):
        super().__init__()

        self.datetime = datetime
        self.point_position = point_position
        self.gts_id = gts_id
        self.value = value

    def get_string(self, short: bool = False):
        _v_ = "/" if self.point_position is None else self.point_position.get_string()
        position_string = _v_

        if short:
            equal_string = "="
            gts_string = ""

        else:
            equal_string = ""
            gts_string = f" {self.gts_id.get_string()}"

        match self.value:
            case bool():
                value_string = "T" if self.value else "F"

            case float() | int() | numbers.Real():
                value_string = str(self.value)

            case str():
                value_string = f"'{u_parse.quote(self.value)}'"

            case _:
                raise TypeError(type(self.value))

        _v_ = "" if self.datetime is None else get_timestamp(self.datetime)
        return f"{equal_string}{_v_}/{position_string}{gts_string} {value_string}"

    def __eq__(self, object):
        if object is self:
            return True

        if type(object) != DataPoint:
            return NotImplemented

        _v_ = object.datetime == self.datetime
        _v_ = _v_ and object.point_position == self.point_position
        return _v_ and object.gts_id == self.gts_id and object.value == self.value

    def __repr__(self):
        return (
            f"DataPoint({repr(self.datetime)}, {repr(self.point_position)},"
            f" {repr(self.gts_id)}, {repr(self.value)})"
        )


def add_data_points(
    data_points: (
        c_abc.Iterable[DataPoint]
        | c_abc.Iterable[tuple["pyarrow.Table", dict[str, GtsId]]]
    ),
    token: str,
    arrow: bool = False,
    address: typing.Any | None = None,
    port: numbers.Integral | int | None = None,
) -> None:
    """
    Adds/updates data points using `http://{address}:{port}/api/v0/update`.

    Parameters
    ----------
    data_points : Iterable[DataPoint] | Iterable[tuple[pyarrow.Table, dict[str, GtsId]]]
        An iterable of data points or an iterable of
        (Pyarrow table, {column name: Geo Time Series id})
    token : str
        An access token
    arrow : bool
        Add from a sequence of Pyarrow tables
    address : Any | None
        A network address, or `None` for `127.0.0.1`
    port : numbers.Integral | int | None
        A network port, or `None` for `8080`
    """
    import pyximport

    pyximport.install()

    import warp10._cython as w__cython
    import httpx

    response = httpx.post(
        f"http://{DEFAULT_ADDRESS if address is None else address}:{DEFAULT_PORT if port is None else port}/api/v0/update",
        headers={"X-Warp10-Token": token},
        content=w__cython.get_line_bytes(
            w__cython.get_arrow__data_points(data_points)
            if arrow
            else w__cython.get__data_points(
                data_points, datetime.datetime(1970, 1, 1).timestamp()
            )
        ),
    )
    if response.status_code != 200:
        raise ApiError(response.status_code, response.text)


class GtsPattern:
    """
    Utility type to select Geo Time Series using Regular Expressions.
    """

    def __init__(
        self, class_pattern: str, label_patterns: c_abc.Mapping[str, str] | None = None
    ):
        """
        Parameters
        ----------
        class_pattern : str
            A Regular Expression pattern
        label_patterns : Mapping[str, str] | None
            A mapping from label names to Regular Expression patterns
        """
        super().__init__()

        self.class_pattern = class_pattern
        self.label_patterns = label_patterns

    def get_string(self):
        class_pattern = ".*" if self.class_pattern is None else self.class_pattern

        if self.label_patterns is None:
            return f"~{class_pattern}{{}}"

        labels_string = ",".join(
            f"{u_parse.quote(name)}~{pattern_string}"
            for name, pattern_string in self.label_patterns.items()
        )
        return f"~{class_pattern}{{{labels_string}}}"


def get_data_points(
    selector: GtsPattern | str,
    token: str,
    start: datetime.datetime | datetime.timedelta | numbers.Integral | int = 1000,
    end: datetime.datetime | None = None,
    build: bool = False,
    address: typing.Any | None = None,
    port: numbers.Integral | int | None = None,
) -> (
    c_abc.Generator[DataPoint, None, None]
    | c_abc.Generator[tuple[GtsId, "pyarrow.Table"], None, None]
):
    """
    Fetches data points using `http://{address}:{port}/api/v0/fetch`.

    Parameters
    ----------
    selector : GtsPattern | str
        A selector for Geo Time Series
    token : str
        An access token
    start : datetime.datetime | datetime.timedelta | numbers.Integral | int
        The left endpoint of the time interval. Either
        - `datetime.datetime`: point
        - `datetime.timedelta` (positive): period before `end`
        - `numbers.Integral | int` (positive): number of data points before `end`
    end : datetime.datetime | None
        The right endpoint of the time interval, or `None` for now.
    build : bool
        Build Pyarrow tables and yield (GtsId, pyarrow.Table) instead. This is
        the most efficient way to load large Geo Time Series
    address : Any | None
        A network address, or `None` for `127.0.0.1`
    port : numbers.Integral | int | None
        A network port, or `None` for `8080`

    Returns
    -------
    data_points : Generator[DataPoint, None, None] | Generator[tuple[GtsId, pyarrow.Table], None, None]
        A generator of data points or a generator of
        (Geo Time Series id, Pyarrow table)
    """
    import pyximport

    pyximport.install()

    import warp10._cython as w__cython
    import httpx

    _v_ = selector.get_string() if isinstance(selector, GtsPattern) else selector
    dictionary = dict(selector=_v_, format="text", showerrors="true")

    if isinstance(start, datetime.datetime):
        dictionary["start"] = get_string(start)[1:-1]

        _v_ = get_string(datetime.datetime.utcnow() if end is None else end)[1:-1]
        dictionary["stop"] = _v_

    else:
        dictionary["now"] = "now" if end is None else str(get_timestamp(end))

        if isinstance(start, numbers.Integral):
            timespan_number = -start

        else:
            _v_ = typing.cast(datetime.timedelta, start).total_seconds() * 1_000_000
            timespan_number = int(_v_)

        dictionary["timespan"] = str(timespan_number)

    _v_ = DEFAULT_ADDRESS if address is None else address
    _v_ = f"http://{_v_}:{DEFAULT_PORT if port is None else port}/api/v0/fetch"
    _v_ = httpx.stream("GET", _v_, headers={"X-Warp10-Token": token}, params=dictionary)
    with _v_ as response:
        _v_ = w__cython.get_generator(response.iter_bytes(BUFFER_SIZE), build=build)
        yield from _v_


def delete_data_points(
    selector: GtsPattern | str,
    token: str,
    start: datetime.datetime | None = None,
    end: datetime.datetime | None = None,
    all: bool = False,
    address: typing.Any | None = None,
    port: numbers.Integral | int | None = None,
    timeout: int = 60,
) -> c_abc.Generator[GtsId, None, None]:
    """
    Deletes data points using `http://{address}:{port}/api/v0/delete`.

    Parameters
    ----------
    selector : GtsPattern | str
        A selector for Geo Time Series
    token : str
        An access token
    start : datetime.datetime | None
        The left endpoint of the time interval, or `None` for unbound.
    end : datetime.datetime | None
        The right endpoint of the time interval, or `None` for unbound.
    all : bool
        Also delete metadata. Requires `start` and `end` to be `None`
    address : Any | None
        A network address, or `None` for `127.0.0.1`
    port : numbers.Integral | int | None
        A network port, or `None` for `8080`
    timeout : int
        Number of seconds to wait for response from server

    Returns
    -------
    gts_ids : Generator[GtsId, None, None]
        A generator of Geo Time Series ids
    """
    import httpx

    _v_ = selector.get_string() if isinstance(selector, GtsPattern) else selector
    dictionary = dict(selector=_v_, showerrors="true")

    if start is None and end is None and all:
        dictionary["deleteall"] = ""

    else:
        if start is None:
            start = get_datetime(0)

        if end is None:
            end = datetime.datetime.max

        if not (get_utc(start) < get_utc(end)):
            return

        dictionary["start"] = get_string(start)[1:-1]
        dictionary["end"] = get_string(end)[1:-1]

    _v_ = DEFAULT_ADDRESS if address is None else address
    _v_ = f"http://{_v_}:{DEFAULT_PORT if port is None else port}/api/v0/delete"
    response = httpx.get(
        _v_, headers={"X-Warp10-Token": token}, params=dictionary, timeout=timeout
    )

    if response.status_code != 200:
        raise ApiError(response.status_code, response.text)

    pattern_string = rf"^{_GTS_PATTERN}\{{\}}$"

    for line_string in response.text.split("\n"):
        if line_string.startswith("# Error:"):
            raise RuntimeError(line_string.removeprefix("# Error:").strip())

        if line_string == "":
            continue

        match = re.search(pattern_string, line_string.rstrip())
        class_string, labels_string = typing.cast(re.Match, match).group("c", "l")

        _v_ = {} if labels_string is None else _get_labels(labels_string)
        yield GtsId(u_parse.unquote(class_string), _v_)


def _get_labels(string):
    dictionary = {}

    for item_string in string.split(","):
        key_string, value_string = item_string.split("=")
        if key_string.startswith("."):
            continue

        dictionary[u_parse.unquote(key_string)] = u_parse.unquote(value_string)

    return dictionary


@dataclasses.dataclass
class Gts:
    """
    Geo Time Series returned by `execute`.
    """

    id: GtsId
    points: list[DataPoint]


@dataclasses.dataclass
class Headers:
    """
    HTTP headers returned by `execute`.
    """

    elapsed: int
    fetched: int
    ops: int
    timeunit: int


def execute(
    code: Literal | str,
    convert_gts: bool = True,
    address: typing.Any | None = None,
    port: numbers.Integral | int | None = None,
) -> tuple[list, Headers]:
    """
    Executes Warpscript code using `http://{address}:{port}/api/v0/exec`.

    Parameters
    ----------
    code : Literal | str
        Warpscript code
    convert_gts : bool
        Convert JSON representation of Geo Time Series to `Gts` objects
    address : Any | None
        A network address, or `None` for `127.0.0.1`
    port : numbers.Integral | int | None
        A network port, or `None` for `8080`

    Returns
    -------
    result : tuple[list, Headers]
        The tuple (stack, response headers)
    """
    import httpx

    _v_ = DEFAULT_ADDRESS if address is None else address
    url_string = f"http://{_v_}:{DEFAULT_PORT if port is None else port}/api/v0/exec"

    response = httpx.post(url_string, content=str(code).encode())
    if response.status_code != 200:
        raise ApiError(response.status_code, response.text)

    stack_list = json.loads(response.text)

    if convert_gts:
        stack_list = typing.cast(list, _convert_gts(stack_list))

    _v_ = Headers(
        elapsed=int(response.headers["x-warp10-elapsed"]),
        fetched=int(response.headers["x-warp10-fetched"]),
        ops=int(response.headers["x-warp10-ops"]),
        timeunit=int(response.headers["x-warp10-timeunit"]),
    )
    return (stack_list, _v_)


def _convert_gts(object):
    if isinstance(object, c_abc.Mapping):
        class_name = object.get("c")
        labels_dictionary = object.get("l")
        values_list = object.get("v")

        _v_ = class_name is not None and labels_dictionary is not None
        if _v_ and values_list is not None:
            _v_ = labels_dictionary.items()
            _v_ = {name: string for name, string in _v_ if not name.startswith(".")}
            gts_id = GtsId(class_name, _v_)

            _v_ = [_convert_tuple(tuple, gts_id) for tuple in values_list]
            return Gts(id=gts_id, points=_v_)

    if isinstance(object, c_abc.Sequence):
        return [_convert_gts(object) for object in object]

    return object


def _convert_tuple(tuple, gts_id):
    if len(tuple) == 2:
        timestamp, value_object = tuple
        point_position = None

    else:
        timestamp, latitude, longitude, elevation, value_object = tuple
        point_position = PointPosition(latitude, longitude, elevation)

    return DataPoint(get_datetime(timestamp), point_position, gts_id, value_object)


class ApiError(Exception):
    """
    Error returned by an API endpoint before execution.
    """

    def __init__(self, status_code, string):
        self.status_code = status_code
        self.string = string

        super().__init__(*self._get_args())

    def _get_args(self):
        match = re.search(
            r"<title>Error\s+(?P<code>\d+)\s+(?P<message>.+?)\s*</title>", self.string
        )

        return (
            (self.status_code, self.string)
            if match is None
            else (int(match.group("code")), match.group("message"))
        )


class RuntimeError(Exception):
    """
    Error returned by an API endpoint during execution.
    """

    def __init__(self, string):
        super().__init__(string)

        self.string = string


def _get_string(args, kwargs, template_tuples, function):
    # find template
    for template_strings, args_indices, kwargs_indices in template_tuples:
        _v_ = len(args_indices) == len(args)
        if _v_ and set(kwargs.keys()).issubset(kwargs_indices.keys()):
            break
    else:
        raise InvalidArguments(args, kwargs, function)

    # get return type
    functions = typing.get_overloads(function)

    _v_ = len(functions) == 0
    _v_ = [function] if _v_ else sorted(functions, key=_get_function_key, reverse=True)
    for function in _v_:
        signature = inspect.signature(function)
        args_parameters = []
        kwargs_parameters = {}
        required_names = set()
        for parameter in signature.parameters.values():
            if parameter.kind == inspect.Parameter.POSITIONAL_ONLY:
                args_parameters.append(parameter)

            else:
                kwargs_parameters[parameter.name] = parameter
                if parameter.default == inspect.Parameter.empty:
                    required_names.add(parameter.name)

        _v_ = len(args_parameters) == len(args)
        if not (_v_ and set(kwargs.keys()).issubset(kwargs_parameters.keys())):
            continue

        for args_parameter, args_object in zip(args_parameters, args):
            if not _is_subclass(_get_type(args_object), args_parameter.annotation):
                break

        else:
            for name, kwargs_parameter in kwargs_parameters.items():
                kwargs_object = kwargs.get(name)
                if kwargs_object is None:
                    if name in required_names:
                        break

                else:
                    _v_ = kwargs_parameter.annotation
                    if not _is_subclass(_get_type(kwargs_object), _v_):
                        break

            else:
                break

    else:
        raise InvalidArguments(args, kwargs, function)

    # fill template
    _v_ = enumerate(args_indices)
    _v_ = [(template_index, 2, args[index]) for index, template_index in _v_]
    tuples = typing.cast(list[tuple[int, int, typing.Any]], _v_)

    _v_ = kwargs_indices.items()
    tuples.extend((template_index, 1, kwargs.get(name)) for name, template_index in _v_)

    tuples.sort(reverse=True)
    strings = list(template_strings)

    for index, length, object in tuples:
        if object is None:
            del strings[index + length - 2 : index + length]

        else:
            strings[index : index + length] = [get_string(object)]

    return Literal(" ".join(strings), type=signature.return_annotation)


def _get_function_key(function):
    parameters = inspect.signature(function).parameters.values()
    args_parameters = []
    kwargs_parameters = {}
    for parameter in parameters:
        match parameter.kind:
            case inspect.Parameter.POSITIONAL_ONLY:
                args_parameters.append(parameter)

            case inspect.Parameter.KEYWORD_ONLY:
                kwargs_parameters[parameter.name] = parameter

            case _:
                raise ValueError(parameter.kind)

    _v_ = sorted(kwargs_parameters.items())
    return (
        -len(parameters),
        [_get_type_key(parameter.annotation) for parameter in args_parameters],
        [_get_type_key(parameter.annotation) for _, parameter in _v_],
    )


def _get_type_key(type):
    if isinstance(type, (types.UnionType, typing._UnionGenericAlias)):
        return min(_get_type_key(type) for type in typing.get_args(type))

    _v_ = [_get_type_key(sub_type) for sub_type in typing.get_args(type)]
    return (_get_type_keys().get(type, 4), _v_)


_TYPE_KEYS = None


def _get_type_keys():
    global _TYPE_KEYS

    if _TYPE_KEYS is not None:
        return _TYPE_KEYS

    import warp10.warpscript as w_warpscript

    _TYPE_KEYS = {w_warpscript._ANY: 0, numbers.Real: 1, numbers.Integral: 2, bool: 3}
    return _TYPE_KEYS


def _get_type(object):
    import warp10.warpscript as w_warpscript

    match object:
        case bool():
            return w_warpscript._BOOLEAN

        case numbers.Integral() | datetime.timedelta():
            return w_warpscript._LONG

        case numbers.Real():
            return w_warpscript._DOUBLE

        case str() | datetime.datetime():
            return w_warpscript._STRING

        case Literal():
            return object.type

        case c_abc.Sequence():
            import warp10.warpscript as w_warpscript

            match object:
                case w_warpscript.Macro():
                    return w_warpscript._MACRO

                case w_warpscript.Vlist():
                    return w_warpscript._VLIST

                case w_warpscript.Star():
                    if len(object) == 0:
                        return w_warpscript.Star

                    return w_warpscript.Star[_get_sub_type(object)]

                case w_warpscript.Plus():
                    if len(object) == 0:
                        return w_warpscript.Plus

                    return w_warpscript.Plus[_get_sub_type(object)]

            _v_ = len(object) == 0
            return (
                w_warpscript._LIST if _v_ else w_warpscript._LIST[_get_sub_type(object)]
            )

        case c_abc.Set():
            _v_ = len(object) == 0
            _v_ = w_warpscript._SET if _v_ else w_warpscript._SET[_get_sub_type(object)]
            return _v_

        case c_abc.Mapping():
            if len(object) == 0:
                return w_warpscript._MAP

            _v_ = _get_sub_type(object.values())
            return w_warpscript._MAP[_get_sub_type(object.keys()), _v_]

    return type(object)


def _get_sub_type(objects):
    return functools.reduce(_get_common_type, (_get_type(object) for object in objects))


def _get_common_type(first_type, second_type):
    first_origin_type, first_args_types = _split_type(first_type)
    second_origin_type, second_args_types = _split_type(second_type)

    first_types = inspect.getmro(first_origin_type)
    common_types = set(first_types).intersection(inspect.getmro(second_origin_type))
    if len(common_types) == 1:
        (common_type,) = common_types

    else:
        common_type = None
        for type in first_types:
            if type in common_types:
                common_type = type
                break

    if not hasattr(common_type, "__class_getitem__"):
        return common_type

    if first_args_types is None:
        if second_args_types is None:
            return common_type

        sub_types = second_args_types

    else:
        if second_args_types is None:
            sub_types = first_args_types

        else:
            _v_ = zip(first_args_types, second_args_types)
            sub_types = tuple(
                _get_common_type(first_sub_type, second_sub_type)
                for first_sub_type, second_sub_type in _v_
            )

    return typing.cast(typing.Any, common_type)[sub_types]


def _is_subclass(class_, classinfo):
    if class_ is None:
        return False

    class_origin_type, class_args_types = _split_type(class_)

    if isinstance(classinfo, typing.TypeVar):
        classinfo = _get_bound_type(classinfo)
        if classinfo is None:
            return True

    _v_ = isinstance(classinfo, (types.UnionType, typing._UnionGenericAlias))
    for classinfo in typing.get_args(classinfo) if _v_ else [classinfo]:
        origin_type, args_types = _split_type(classinfo)

        _v_ = issubclass(class_origin_type, origin_type) and not (
            issubclass(class_origin_type, str)
            and issubclass(origin_type, c_abc.Sequence)
            and not issubclass(origin_type, str)
        )
        if not _v_:
            continue

        if not (args_types is not None and class_args_types is not None):
            return True

        if all(
            _is_subclass(class_args_type, args_type)
            for args_type, class_args_type in zip(args_types, class_args_types)
        ):
            return True

    return False


_BOUND_TYPES = {}


def _get_bound_type(type_var):
    bound_object = type_var.__bound__
    if isinstance(bound_object, typing.ForwardRef):
        type = _BOUND_TYPES.get(bound_object, NONE)
        if type is not NONE:
            return type

        _v_ = vars(importlib.import_module(type_var.__module__))
        type = bound_object._evaluate(globals(), _v_, frozenset())

        _BOUND_TYPES[bound_object] = type
        return type

    return bound_object


def _split_type(type):
    if isinstance(type, (types.GenericAlias, typing._GenericAlias)):
        return (typing.get_origin(type), typing.get_args(type))

    return (type, None)


class InvalidArguments(Exception):
    """
    Error when type checks fail at runtime.
    """

    def __init__(self, args, kwargs, function):
        super().__init__()

        self.args = args
        self.kwargs = kwargs
        self.function = function

    def __str__(self):
        argument_strings = [repr(object) for object in self.args]

        _v_ = (f"{name}={repr(object)}" for name, object in self.kwargs.items())
        argument_strings.extend(_v_)

        arguments_string = ", ".join(argument_strings)
        return f"{self.function.__name__}({arguments_string})"


def _stack(args, name, type):
    string = " ".join(get_string(object) for object in args)
    return Literal(f"{string} {name}", type=type)


_get_2d_T = typing.TypeVar("_get_2d_T")


def get_2d(
    data_points: c_abc.Iterable[DataPoint],
    type: c_abc.Callable[[c_abc.Mapping[str, c_abc.Sequence]], _get_2d_T],
) -> c_abc.Generator[tuple[GtsId, _get_2d_T], None, None]:
    """
    Converts sequences of data points with the same Geo Time Series id into
    2-dimensional objects with columns `"datetime"`, `"latitude"`,
    `"longitude"`, `"elevation"` and `"value"`.

    See `warp10.arrow.get_tables` and `warp10.pandas.get_data_frames`.

    Parameters
    ----------
    data_points : Iterable[DataPoint]
        A sequence of data points
    type : Callable[[Mapping[str, Sequence]], T]
        A function which converts a dictionary of lists into a 2-dimensional
        object

    Returns
    -------
    tuples : Generator[tuple[GtsId, T], None, None]
        A generator of (Geo Time Series id, 2-dimensional object)
    """

    gts_id = None
    datetimes = []
    latitudes = []
    longitudes = []
    elevations = []
    value_objects = []

    for data_point in data_points:
        if data_point.gts_id != gts_id:
            if gts_id is not None:
                _v_ = dict(
                    datetime=datetimes,
                    latitude=latitudes,
                    longitude=longitudes,
                    elevation=elevations,
                    value=value_objects,
                )
                yield (gts_id, type(_v_))

                datetimes.clear()
                latitudes.clear()
                longitudes.clear()
                elevations.clear()
                value_objects.clear()

            gts_id = data_point.gts_id

        datetimes.append(data_point.datetime)

        point_position = data_point.point_position
        if point_position is None:
            latitudes.append(None)
            longitudes.append(None)
            elevations.append(None)

        else:
            latitudes.append(point_position.latitude)
            longitudes.append(point_position.longitude)
            elevations.append(point_position.elevation)

        value_objects.append(data_point.value)

    if gts_id is not None:
        _v_ = dict(
            datetime=datetimes,
            latitude=latitudes,
            longitude=longitudes,
            elevation=elevations,
            value=value_objects,
        )
        yield (gts_id, type(_v_))


_merge_2d_T = typing.TypeVar("_merge_2d_T")


def merge_2d(
    _2ds: c_abc.Iterable[tuple[GtsId, _merge_2d_T]],
    type: c_abc.Callable[[c_abc.Mapping[str | GtsId, c_abc.Sequence]], _merge_2d_T],
    join: c_abc.Callable[[_merge_2d_T, _merge_2d_T], _merge_2d_T] | None = None,
    equal: c_abc.Callable[[c_abc.Sequence, c_abc.Sequence | None], bool] = operator.eq,
) -> c_abc.Generator[_merge_2d_T, None, None]:
    """
    Converts sequences of (Geo Time Series id, 2-dimensional object) with the
    same datetime, latitude, longitude and elevation columns into 2-dimensional
    objects with all value columns.

    See `warp10.arrow.merge_tables` and `warp10.pandas.merge_data_frames`.

    Parameters
    ----------
    _2ds : Iterable[tuple[GtsId, T]],
        A sequence of (Geo Time Series id, 2-dimensional object)
    type : Callable[[Mapping[str | GtsId, Sequence]], T]
        A function which converts a dictionary of lists into a 2-dimensional
        object
    join : Callable[[T, T], T] | None
        A function which joins two 2-dimensional objects. If `None`, doesn't join
    equal : Callable[[Sequence, Sequence | None], bool]
        A function which returns `True` if two sequences are equal

    Returns
    -------
    2-dimensional objects : Generator[T, None, None]
        A generator of 2-dimensional objects
    """
    datetimes = None
    latitudes = None
    longitudes = None
    elevations = None
    tuples = []
    joined_2d = None

    for tuple in _2ds:
        _, _2d = tuple
        _2d = typing.cast(c_abc.Mapping[str, c_abc.Sequence], _2d)

        _v_ = equal(_2d["datetime"], datetimes) and equal(_2d["latitude"], latitudes)
        _v_ = _v_ and equal(_2d["longitude"], longitudes)
        if not (_v_ and equal(_2d["elevation"], elevations)):
            if len(tuples) != 0:
                dictionary = dict(
                    datetime=datetimes,
                    latitude=latitudes,
                    longitude=longitudes,
                    elevation=elevations,
                )
                dictionary.update((gts_id, _2d["value"]) for gts_id, _2d in tuples)
                merged_2d = type(typing.cast(typing.Any, dictionary))
                if join is None:
                    yield merged_2d

                else:
                    _v_ = merged_2d if joined_2d is None else join(joined_2d, merged_2d)
                    joined_2d = _v_

                tuples.clear()

            datetimes = _2d["datetime"]
            latitudes = _2d["latitude"]
            longitudes = _2d["longitude"]
            elevations = _2d["elevation"]

        tuples.append(tuple)

    if len(tuples) != 0:
        dictionary = dict(
            datetime=datetimes,
            latitude=latitudes,
            longitude=longitudes,
            elevation=elevations,
        )
        dictionary.update((gts_id, _2d["value"]) for gts_id, _2d in tuples)
        merged_2d = type(typing.cast(typing.Any, dictionary))
        if join is None:
            yield merged_2d

        else:
            yield merged_2d if joined_2d is None else join(merged_2d, joined_2d)
