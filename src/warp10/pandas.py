import warp10
import numpy
import pandas
import collections.abc as c_abc
import typing


def get_data_frames(
    data_points: c_abc.Iterable[warp10.DataPoint],
) -> c_abc.Generator[tuple[warp10.GtsId, pandas.DataFrame], None, None]:
    """
    `warp10.get_2d` yielding `pandas.DataFrame`.

    Parameters
    ----------
    data_points : Iterable[warp10.DataPoint]
        A sequence of data points

    Returns
    -------
    tuples : Generator[tuple[warp10.GtsId, pandas.DataFrame], None, None]
        A generator of (Geo Time Series id, Pandas data frame)
    """
    _v_ = typing.cast(
        c_abc.Callable[[c_abc.Mapping[str, c_abc.Sequence]], pandas.DataFrame],
        _create_data_frame,
    )
    yield from warp10.get_2d(data_points, _v_)


def merge_data_frames(
    data_frames: c_abc.Iterable[tuple[warp10.GtsId, pandas.DataFrame]],
    join: bool = True,
) -> c_abc.Generator[pandas.DataFrame, None, None]:
    """
    `warp10.merge_2d` yielding `pandas.DataFrame`.

    Parameters
    ----------
    data_frames : Iterable[tuple[warp10.GtsId, pandas.DataFrame]]
        A sequence of (Geo Time Series id, Pandas data frame)
    join : bool
        Join data frames

    Returns
    -------
    data frames : Generator[pandas.DataFrame, None, None]
        A generator of Pandas data frames
    """
    _v_ = typing.cast(
        c_abc.Callable[
            [c_abc.Mapping[str | warp10.GtsId, c_abc.Sequence]], pandas.DataFrame
        ],
        _create_data_frame,
    )
    yield from warp10.merge_2d(
        data_frames, _v_, join=_join_data_frames if join else None, equal=_equal_series
    )


_TYPES = dict(
    datetime="datetime64[us, UTC]",
    latitude=numpy.float64,
    longitude=numpy.float64,
    elevation=pandas.Int64Dtype(),
)


def _create_data_frame(dictionary):
    return pandas.DataFrame.from_dict(dictionary).astype(_TYPES, copy=False)


def _join_data_frames(first_data_frame, second_data_frame):
    _v_ = ["datetime", "latitude", "longitude", "elevation"]
    return first_data_frame.merge(second_data_frame, how="outer", on=_v_)


def _equal_series(first_series, second_series):
    return first_series.equals(second_series)
