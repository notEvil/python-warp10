import warp10
import pyarrow
import collections.abc as c_abc


def get_tables(
    data_points: c_abc.Iterable[warp10.DataPoint],
) -> c_abc.Generator[tuple[warp10.GtsId, pyarrow.Table], None, None]:
    """
    `warp10.get_2d` yielding `pyarrow.Table`.

    Parameters
    ----------
    data_points : Iterable[warp10.DataPoint]
        A sequence of data points

    Returns
    -------
    tuples : Generator[tuple[warp10.GtsId, pyarrow.Table], None, None]
        A generator of (Geo Time Series id, Pyarrow table)
    """
    yield from warp10.get_2d(data_points, _create_table)


def merge_tables(
    tables: c_abc.Iterable[tuple[warp10.GtsId, pyarrow.Table]], join: bool = True
) -> c_abc.Generator[pyarrow.Table, None, None]:
    """
    `warp10.merge_2d` yielding `pyarrow.Table`.

    Parameters
    ----------
    tables : Iterable[tuple[warp10.GtsId, pyarrow.Table]]
        A sequence of (Geo Time Series id, Pyarrow table)
    join : bool
        Join tables

    Returns
    -------
    tables : Generator[pyarrow.Table, None, None]
        A generator of Pyarrow tables
    """
    _v_ = warp10.merge_2d(tables, _create_table, join=_join_tables if join else None)
    yield from _v_


_TYPES = dict(
    datetime=pyarrow.timestamp("us", tz="UTC"),
    latitude=pyarrow.float64(),
    longitude=pyarrow.float64(),
    elevation=pyarrow.int64(),
)


def _create_table(dictionary):
    _v_ = {
        column_id if isinstance(column_id, str) else str(column_id): (
            objects
            if isinstance(objects, (pyarrow.Array, pyarrow.ChunkedArray))
            else pyarrow.array(objects, type=_TYPES.get(column_id))
        )
        for column_id, objects in dictionary.items()
    }
    return pyarrow.table(_v_)


def _join_tables(first_table, second_table):
    _v_ = {
        column_name: array
        for column_name, array in zip(second_table.column_names, second_table.columns)
        if not (array.null_count == len(array) and column_name in _TYPES)
    }
    second_table = pyarrow.table(_v_)

    _v_ = list(set(_TYPES).intersection(second_table.column_names))
    return first_table.join(second_table, _v_, join_type="full outer")
