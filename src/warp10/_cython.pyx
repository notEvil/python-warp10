# cython: language_level=3
# distutils: language = c++

import pyarrow
import warp10
import datetime
import urllib.parse as u_parse

cimport cython
cimport libc.math as l_math
cimport libc.stdint as l_stdint
cimport libcpp
cimport libcpp.string as libcpp_string


cdef extern from "<system_error>" namespace "std":
    cdef enum class errc:
        pass


cdef extern from "<charconv>" namespace "std":
    cdef cppclass from_chars_result:
        const char* ptr
        errc ec

    cdef from_chars_result from_chars(const char* f, const char* l, l_stdint.int64_t& v)
    cdef from_chars_result from_chars(const char* f, const char* l, double& v)


cdef enum _Type:
    _NULL, _BOOLEAN, _INT64, _DOUBLE, _STRING, _TIMESTAMP


cdef class _DataPoint:
    cdef bint equal
    cdef _Type timestamp_type
    cdef l_stdint.int64_t timestamp
    cdef _Type latitude_type
    cdef double latitude
    cdef _Type longitude_type
    cdef double longitude
    cdef _Type elevation_type
    cdef l_stdint.int64_t elevation
    cdef object gts_id
    cdef _Type value_type
    cdef void* value_object
    cdef libcpp.bool b
    cdef l_stdint.int64_t i
    cdef double d
    cdef str s


class BuildError(Exception):
    pass


def get_generator(bytes_iterator, bint build = False):
    cdef const l_stdint.uint8_t[:] array
    cdef int length
    cdef int start_index
    cdef int end_index
    cdef bint join = False
    cdef _DataPoint _data_point = _DataPoint()
    cdef object value_object

    parts = []

    cdef Builder b = Builder(_NULL)
    cdef Builder datetime_builder = b
    cdef Builder latitude_builder = b
    cdef Builder longitude_builder = b
    cdef Builder elevation_builder = b
    cdef Builder value_builder = b
    cdef int failed = 0

    gts_id = None

    for array in bytes_iterator:
        length = array.shape[0]
        start_index = 0

        while True:
            end_index = _find(array, start_index, length, b"\n")
            if end_index == length:
                parts.append(array[start_index:])
                join = True
                break

            if join: 
                parts.append(array[start_index:end_index])
                bytes = b"".join(parts)
                parts.clear()
                join = False

                _get_data_point(bytes, 0, len(bytes), _data_point)

            else:
                _get_data_point(array, start_index, end_index, _data_point)

            if build:
                if not _data_point.equal:
                    if failed:
                        raise BuildError(failed)

                    if gts_id is not None:
                        yield (
                            gts_id,
                            _get_table(
                                datetime_builder,
                                latitude_builder,
                                longitude_builder,
                                elevation_builder,
                                value_builder,
                            ),
                        )

                    gts_id = _data_point.gts_id
                    datetime_builder = Builder(_TIMESTAMP)
                    latitude_builder = Builder(_DOUBLE)
                    longitude_builder = Builder(_DOUBLE)
                    elevation_builder = Builder(_INT64)
                    value_builder = Builder(_NULL)

                failed |= datetime_builder.add(&_data_point.timestamp, _data_point.timestamp_type)
                failed |= latitude_builder.add(&_data_point.latitude, _data_point.latitude_type)
                failed |= longitude_builder.add(&_data_point.longitude, _data_point.longitude_type)
                failed |= elevation_builder.add(&_data_point.elevation, _data_point.elevation_type)
                failed |= value_builder.add(_data_point.value_object, _data_point.value_type)

            else:
                if _data_point.value_type == _DOUBLE:
                    value_object = _data_point.d

                elif _data_point.value_type == _INT64:
                    value_object = _data_point.i

                elif _data_point.value_type == _BOOLEAN:
                    value_object = _data_point.b

                elif _data_point.value_type == _STRING:
                    value_object = _data_point.s

                else:
                    value_object = None

                yield warp10.DataPoint(
                    (
                        None
                        if _data_point.timestamp_type == _NULL
                        else datetime.datetime.fromtimestamp(
                            _data_point.timestamp * 1e-6, tz=datetime.timezone.utc
                        )
                    ),
                    (
                        None
                        if _data_point.latitude_type == _NULL
                        and _data_point.elevation_type == _NULL
                        else warp10.PointPosition(
                            (
                                None
                                if _data_point.latitude_type == _NULL
                                else _data_point.latitude
                            ),
                            (
                                None
                                if _data_point.longitude_type == _NULL
                                else _data_point.longitude
                            ),
                            (
                                None
                                if _data_point.elevation_type == _NULL
                                else _data_point.elevation
                            ),
                        )
                    ),
                    _data_point.gts_id,
                    value_object,
                )

            start_index = end_index + 1

        if failed:
            raise BuildError(failed)

    if gts_id is not None:
        yield (
            gts_id,
            _get_table(
                datetime_builder,
                latitude_builder,
                longitude_builder,
                elevation_builder,
                value_builder,
            ),
        )


@cython.boundscheck(False)
@cython.wraparound(False)
cdef void _get_data_point(const l_stdint.uint8_t[:] _bytes, int index, int end_index, _DataPoint _data_point):
    cdef from_chars_result result

    _data_point.equal = _bytes[index] == b"="
    index += _data_point.equal  # =

    # timestamp
    result = from_chars(<const char*>&_bytes[index], <const char*>&_bytes[end_index], _data_point.timestamp)
    if result.ec == <errc>0:
        _data_point.timestamp_type = _TIMESTAMP
        index = result.ptr - <const char*>&_bytes[0]

    else:
        _data_point.timestamp_type = _NULL
    #

    index += 1  # /
    _get_point_position(_bytes, index, end_index, _data_point)
    index += 1  # space

    if not _data_point.equal:
        class_name = _get_string(_bytes, index, end_index, b"{")
        index += 1  # {
        labels_dictionary = {}

        while _bytes[index] != b"}":
            key_string = _get_string(_bytes, index, end_index, b"=")
            index += 1  # =
            value_string = _get_string(_bytes, index, end_index, b",}")

            if not key_string.startswith("."):
                labels_dictionary[key_string] = value_string

            index += _bytes[index] == b","  # ,

        _data_point.gts_id = warp10.GtsId(class_name, labels_dictionary)
        index += 2  # } space

    character = _bytes[index]
    if (b"0" <= character and character <= b"9") or character == b"-":
        if _find(_bytes, index + 1, end_index, b".E") == end_index:
            result = from_chars(<const char*>&_bytes[index], <const char*>&_bytes[end_index], _data_point.i)
            if result.ec == <errc>0:
                _data_point.value_type = _INT64
                _data_point.value_object = &_data_point.i

            elif _compare(_bytes[index + 1:], b"Infinity\n"):
                _data_point.d = -l_math.INFINITY
                _data_point.value_type = _DOUBLE
                _data_point.value_object = &_data_point.d

            else:
                _data_point.value_type = _NULL

        else:
            result = from_chars(<const char*>&_bytes[index], <const char*>&_bytes[end_index], _data_point.d)
            if result.ec == <errc>0:
                _data_point.value_type = _DOUBLE
                _data_point.value_object = &_data_point.d

            else:
                _data_point.value_type = _NULL

    elif character == b"F":
        _data_point.b = False
        _data_point.value_type = _BOOLEAN
        _data_point.value_object = &_data_point.b

    elif character == b"T":
        _data_point.b = True
        _data_point.value_type = _BOOLEAN
        _data_point.value_object = &_data_point.b

    elif character == b"'":
        _data_point.s = u_parse.unquote(bytes(_bytes[index + 1 : end_index - 1]))
        _data_point.value_type = _STRING
        _data_point.value_object = <void*>_data_point.s

    elif _compare(_bytes[index:], b"Infinity\n"):
        _data_point.d = l_math.INFINITY
        _data_point.value_type = _DOUBLE
        _data_point.value_object = &_data_point.d

    elif _compare(_bytes[index:], b"NaN\n"):
        _data_point.d = l_math.NAN
        _data_point.value_type = _DOUBLE
        _data_point.value_object = &_data_point.d

    else:
        _data_point.value_type = _NULL


@cython.boundscheck(False)
@cython.wraparound(False)
cdef void _get_point_position(const l_stdint.uint8_t[:] bytes, int& index, int end_index, _DataPoint _data_point):
    cdef from_chars_result result

    if bytes[index] == b"/":
        _data_point.latitude_type = _NULL
        _data_point.longitude_type = _NULL

    else:
        # latitude
        result = from_chars(<const char*>&bytes[index], <const char*>&bytes[end_index], _data_point.latitude)
        if result.ec == <errc>0:
            _data_point.latitude_type = _DOUBLE
            index = result.ptr - <const char*>&bytes[0]

        else:
            _data_point.latitude_type = _NULL
        #

        index += 1  # :

        # longitude
        result = from_chars(<const char*>&bytes[index], <const char*>&bytes[end_index], _data_point.longitude)
        if result.ec == <errc>0:
            _data_point.longitude_type = _DOUBLE
            index = result.ptr - <const char*>&bytes[0]

        else:
            _data_point.longitude_type = _NULL

    index += 1  # /

    # elevation
    result = from_chars(<const char*>&bytes[index], <const char*>&bytes[end_index], _data_point.elevation)
    if result.ec == <errc>0:
        _data_point.elevation_type = _DOUBLE
        index = result.ptr - <const char*>&bytes[0]

    else:
        _data_point.elevation_type = _NULL


@cython.boundscheck(False)
@cython.wraparound(False)
cdef str _get_string(const l_stdint.uint8_t[:] _bytes, int& index, int end_index, const l_stdint.uint8_t[] end_characters):
    cdef int start_index = index
    index = _find(_bytes, start_index, end_index, end_characters)  # str
    return u_parse.unquote(bytes(_bytes[start_index: index]))


cdef object _get_table(Builder datetime_builder, Builder latitude_builder, Builder longitude_builder, Builder elevation_builder, Builder value_builder):
    datetime_array = datetime_builder.finish()
    if datetime_array is None:
        raise BuildError(1)

    latitude_array = latitude_builder.finish()
    if latitude_array is None:
        raise BuildError(1)

    longitude_array = longitude_builder.finish()
    if longitude_array is None:
        raise BuildError(1)

    elevation_array = elevation_builder.finish()
    if elevation_array is None:
        raise BuildError(1)

    value_array = value_builder.finish()
    if value_array is None:
        raise BuildError(1)
    
    return pyarrow.table(
        dict(
            datetime=datetime_array,
            latitude=latitude_array,
            longitude=longitude_array,
            elevation=elevation_array,
            value=value_array,
        )
    )


@cython.boundscheck(False)
@cython.wraparound(False)
cdef int _find(const l_stdint.uint8_t[:] bytes, int start_index, int end_index, const l_stdint.uint8_t[] characters) noexcept:
    cdef int index = start_index
    while index < end_index:
        bytes_character = bytes[index]
        for characters_index in range(len(characters)):
            if characters[characters_index] == bytes_character:
                return index

        index += 1

    return end_index


@cython.boundscheck(False)
@cython.wraparound(False)
cdef bint _compare(const l_stdint.uint8_t[:] first, const l_stdint.uint8_t[:] second) noexcept:
    cdef int length = first.shape[0]
    if second.shape[0] != length:
        return False

    for index in range(length):
        if first[index] != second[index]:
            return False

    return True


cimport cython.operator as c_operator
cimport libcpp.memory as l_memory
cimport libcpp.optional as l_optional
cimport pyarrow.lib as p_lib


cdef extern from "arrow/status.h" namespace "arrow":
    cdef cppclass Status:  # return value
        libcpp.bool ok() const


# TimestampBuilder
cdef extern from "arrow/type_fwd.h" namespace "arrow":
    cdef enum class TimeUnit_type "arrow::TimeUnit::type":
        MICRO

    cdef cppclass MemoryPool:
        pass

    cdef MemoryPool* default_memory_pool()


cdef extern from "arrow/type.h" namespace "arrow":
    cdef cppclass DataType:
        pass

    cdef cppclass TimestampType:
        TimestampType(TimeUnit_type u, const libcpp_string.string& t)


# arrays
cdef extern from "arrow/array/array_base.h" namespace "arrow":
    cdef cppclass NullArray:
        pass


cdef extern from "<string_view>" namespace "std":
    cdef cppclass string_view:
        const char* data() const
        size_t length() const


cdef extern from "arrow/stl_iterator.h" namespace "arrow::stl":
    cdef cppclass BooleanIterator "arrow::stl::ArrayIterator<arrow::BooleanArray>":
        l_optional.optional[libcpp.bool] operator*() const
        BooleanIterator& operator++(int)

    cdef cppclass Int64Iterator "arrow::stl::ArrayIterator<arrow::Int64Array>":
        libcpp.bool operator!=(const Int64Iterator& o) const
        l_optional.optional[l_stdint.int64_t] operator*() const
        Int64Iterator& operator++()
        Int64Iterator& operator++(int)

    cdef cppclass DoubleIterator "arrow::stl::ArrayIterator<arrow::DoubleArray>":
        l_optional.optional[double] operator*() const
        DoubleIterator& operator++(int)

    cdef cppclass StringIterator "arrow::stl::ArrayIterator<arrow::BaseBinaryArray<arrow::BinaryType>>":
        l_optional.optional[string_view] operator*() const
        StringIterator& operator++(int)

    cdef cppclass TimestampIterator "arrow::stl::ArrayIterator<arrow::TimestampArray>":
        libcpp.bool operator!=(const TimestampIterator& o) const
        l_optional.optional[l_stdint.int64_t] operator*() const
        TimestampIterator& operator++(int)

    cdef cppclass BooleanChunkedIterator "arrow::stl::ChunkedArrayIterator<arrow::BooleanArray>":
        BooleanChunkedIterator()
        BooleanChunkedIterator(p_lib.CChunkedArray& c, l_stdint.int64_t i)
        l_optional.optional[libcpp.bool] operator*() const
        BooleanChunkedIterator& operator++(int)

    cdef cppclass Int64ChunkedIterator "arrow::stl::ChunkedArrayIterator<arrow::Int64Array>":
        Int64ChunkedIterator()
        Int64ChunkedIterator(p_lib.CChunkedArray& c, l_stdint.int64_t i)
        l_optional.optional[l_stdint.int64_t] operator*() const
        Int64ChunkedIterator& operator++(int)

    cdef cppclass DoubleChunkedIterator "arrow::stl::ChunkedArrayIterator<arrow::DoubleArray>":
        DoubleChunkedIterator()
        DoubleChunkedIterator(p_lib.CChunkedArray& c, l_stdint.int64_t i)
        l_optional.optional[double] operator*() const
        DoubleChunkedIterator& operator++(int)

    cdef cppclass StringChunkedIterator "arrow::stl::ChunkedArrayIterator<arrow::StringArray>":
        StringChunkedIterator()
        StringChunkedIterator(p_lib.CChunkedArray& c, l_stdint.int64_t i)
        l_optional.optional[string_view] operator*() const
        StringChunkedIterator& operator++(int)

    cdef cppclass TimestampChunkedIterator "arrow::stl::ChunkedArrayIterator<arrow::TimestampArray>":
        TimestampChunkedIterator()
        TimestampChunkedIterator(p_lib.CChunkedArray& c, l_stdint.int64_t i)
        libcpp.bool operator!=(const TimestampChunkedIterator& o) const
        l_optional.optional[l_stdint.int64_t] operator*() const
        TimestampChunkedIterator& operator++(int)
        l_stdint.int64_t index() const


cdef extern from "arrow/array/array_primitive.h" namespace "arrow":
    cdef cppclass BooleanArray:
        BooleanIterator begin() const
        BooleanIterator end() const

    cdef cppclass Int64Array:
        Int64Iterator begin() const
        Int64Iterator end() const

    cdef cppclass DoubleArray:
        DoubleIterator begin() const
        DoubleIterator end() const

    cdef cppclass TimestampArray:
        TimestampIterator begin() const
        TimestampIterator end() const


cdef extern from "arrow/array/array_binary.h" namespace "arrow":
    cdef cppclass StringArray:
        StringIterator begin() const
        StringIterator end() const


# builder
cdef extern from "arrow/array/builder_primitive.h" namespace "arrow":
    cdef cppclass NullBuilder:
        NullBuilder()
        l_stdint.int64_t length() const
        Status AppendNull()
        Status Finish(l_memory.shared_ptr[NullArray]* out)

    cdef cppclass BooleanBuilder:
        BooleanBuilder()
        Status Append(const libcpp.bool v)
        Status AppendNull()
        Status AppendNulls(l_stdint.int64_t length)
        Status Finish(l_memory.shared_ptr[BooleanArray]* out)

    cdef cppclass Int64Builder:
        Int64Builder()
        Status Append(const l_stdint.int64_t v)
        Status AppendNull()
        Status AppendNulls(l_stdint.int64_t length)
        Status Finish(l_memory.shared_ptr[Int64Array]* out)

    cdef cppclass DoubleBuilder:
        DoubleBuilder()
        Status Append(const double v)
        Status AppendNull()
        Status AppendNulls(l_stdint.int64_t length)
        Status Finish(l_memory.shared_ptr[DoubleArray]* out)

    cdef cppclass TimestampBuilder:
        TimestampBuilder(const l_memory.shared_ptr[DataType]& t, MemoryPool* p)
        Status Append(const l_stdint.int64_t v)
        Status AppendNull()
        Status AppendNulls(l_stdint.int64_t length)
        Status Finish(l_memory.shared_ptr[TimestampArray]* out)


cdef extern from "arrow/array/builder_binary.h" namespace "arrow":
    cdef cppclass StringBuilder:
        StringBuilder()
        Status Append(const l_stdint.uint8_t* v, size_t l)
        Status AppendNull()
        Status AppendNulls(l_stdint.int64_t length)
        Status Finish(l_memory.shared_ptr[StringArray]* out)
#


cdef class Builder:
    cdef _Builder _builder
    cdef _Type _type

    def __cinit__(self, _Type type):
        self._builder = _BUILDER[type]()
        self._type = type

    cdef int add(self, void* object, _Type type):
        cdef l_memory.shared_ptr[Int64Array] array
        cdef int failed = self._builder.add(object, type)

        if not failed:
            return 0

        if failed == 2:
            if self._type == _NULL:
                length = (<_NullBuilder>self._builder)._builder.length()

                builder_type = _BUILDER.get(type)
                if builder_type is None:
                    return 4

                self._builder = builder_type()
                self._type = type

                failed = self._builder._add_nulls(length)
                if failed:
                    return failed

                return self._builder.add(object, type)

            elif self._type == _INT64 and type == _DOUBLE:
                if not (<_Int64Builder>self._builder)._builder.Finish(&array).ok():
                    return 8

                self._builder = _DoubleBuilder()

                for optional in c_operator.dereference(array):
                    if optional.has_value():
                        failed = self._builder.add(&optional.value(), _INT64)
                        if failed:
                            return failed

                    else:
                        failed = self._builder.add(NULL, _NULL)
                        if failed:
                            return failed

                return self._builder.add(object, type)

        return failed

    cdef object finish(self):
        cdef l_memory.shared_ptr[p_lib.CArray] array
        return None if self._builder.finish(array) else p_lib.pyarrow_wrap_array(array)


cdef class _Builder:
    cdef int add(self, void* object, _Type type):
        return 1 << 30

    cdef int _add_nulls(self, l_stdint.int64_t length):
        return 1 << 30

    cdef bint finish(self, l_memory.shared_ptr[p_lib.CArray]& array):
        return True


cdef class _NullBuilder(_Builder):
    cdef NullBuilder _builder

    cdef int add(self, void* object, _Type type):
        if type == _NULL:
            return not self._builder.AppendNull().ok()

        return 2

    cdef bint finish(self, l_memory.shared_ptr[p_lib.CArray]& array):
        return not self._builder.Finish(<l_memory.shared_ptr[NullArray]*>&array).ok()


cdef class _BooleanBuilder(_Builder):
    cdef BooleanBuilder _builder

    cdef int add(self, void* object, _Type type):
        if type == _BOOLEAN:
            return not self._builder.Append((<libcpp.bool*>object)[0]).ok()

        if type == _NULL:
            return not self._builder.AppendNull().ok()

        return 2

    cdef int _add_nulls(self, l_stdint.int64_t length):
        return not self._builder.AppendNulls(length).ok()

    cdef bint finish(self, l_memory.shared_ptr[p_lib.CArray]& array):
        return not self._builder.Finish(<l_memory.shared_ptr[BooleanArray]*>&array).ok()


cdef class _Int64Builder(_Builder):
    cdef Int64Builder _builder

    cdef int add(self, void* object, _Type type):
        if type == _INT64:
            return not self._builder.Append((<l_stdint.int64_t*>object)[0]).ok()

        if type == _NULL:
            return not self._builder.AppendNull().ok()

        return 2

    cdef int _add_nulls(self, l_stdint.int64_t length):
        return not self._builder.AppendNulls(length).ok()

    cdef bint finish(self, l_memory.shared_ptr[p_lib.CArray]& array):
        return not self._builder.Finish(<l_memory.shared_ptr[Int64Array]*>&array).ok()


cdef class _DoubleBuilder(_Builder):
    cdef DoubleBuilder _builder

    cdef int add(self, void* object, _Type type):
        if type == _DOUBLE:
            return not self._builder.Append((<double*>object)[0]).ok()

        if type == _NULL:
            return not self._builder.AppendNull().ok()

        if type == _INT64:
            return not self._builder.Append((<l_stdint.int64_t*>object)[0]).ok()

        return 2

    cdef int _add_nulls(self, l_stdint.int64_t length):
        return not self._builder.AppendNulls(length).ok()

    cdef bint finish(self, l_memory.shared_ptr[p_lib.CArray]& array):
        return not self._builder.Finish(<l_memory.shared_ptr[DoubleArray]*>&array).ok()


cdef class _StringBuilder(_Builder):
    cdef StringBuilder _builder

    @cython.boundscheck(False)
    cdef int add(self, void* object, _Type type):
        cdef const l_stdint.uint8_t[:] array
        if type == _STRING:
            array = (<str>object).encode()
            return not self._builder.Append(&array[0], array.shape[0]).ok()

        if type == _NULL:
            return not self._builder.AppendNull().ok()

        return 2

    cdef int _add_nulls(self, l_stdint.int64_t length):
        return not self._builder.AppendNulls(length).ok()

    cdef bint finish(self, l_memory.shared_ptr[p_lib.CArray]& array):
        return not self._builder.Finish(<l_memory.shared_ptr[StringArray]*>&array).ok()


cdef class _TimestampBuilder(_Builder):
    cdef TimestampBuilder* _builder

    def __cinit__(self):
        self._builder = new TimestampBuilder(<l_memory.shared_ptr[DataType]>l_memory.make_shared[TimestampType](TimeUnit_type.MICRO, b"UTC"), default_memory_pool())

    def __dealloc__(self):
        del self._builder

    cdef int add(self, void* object, _Type type):
        if type == _TIMESTAMP:
            return not self._builder.Append((<l_stdint.int64_t*>object)[0]).ok()

        if type == _NULL:
            return not self._builder.AppendNull().ok()

        return 2

    cdef int _add_nulls(self, l_stdint.int64_t length):
        return not self._builder.AppendNulls(length).ok()

    cdef bint finish(self, l_memory.shared_ptr[p_lib.CArray]& array):
        return not self._builder.Finish(<l_memory.shared_ptr[TimestampArray]*>&array).ok()


_BUILDER = {
    _NULL: _NullBuilder,
    _BOOLEAN: _BooleanBuilder,
    _INT64: _Int64Builder,
    _DOUBLE: _DoubleBuilder,
    _STRING: _StringBuilder,
    _TIMESTAMP: _TimestampBuilder,
}


import numbers

cimport libc.string as libc_string


cdef extern from "<charconv>" namespace "std":
    cdef cppclass to_chars_result:
        const char* ptr
        errc ec

    cdef to_chars_result to_chars(char* f, char* l, double v)


def get__data_points(data_points, int _zero):
    cdef _DataPoint _data_point = _DataPoint()

    for data_point in data_points:
        gts_id = data_point.gts_id
        if gts_id is _data_point.gts_id:
            _data_point.equal = True

        else:
            _data_point.equal = gts_id == _data_point.gts_id
            _data_point.gts_id = gts_id

        datetime = data_point.datetime
        if datetime is None:
            _data_point.timestamp_type = _NULL

        else:
            _data_point.timestamp_type = _TIMESTAMP
            _data_point.timestamp = <l_stdint.int64_t>((<double>float(datetime.timestamp()) - (_zero if datetime.tzinfo is None else 0)) * 1e6)

        point_position = data_point.point_position
        if point_position is None:
            _data_point.latitude_type = _NULL
            _data_point.longitude_type = _NULL
            _data_point.elevation_type = _NULL

        else:
            latitude = point_position.latitude
            if latitude is None:
                _data_point.latitude_type = _NULL
                _data_point.longitude_type = _NULL

            else:
                _data_point.latitude_type = _DOUBLE
                _data_point.latitude = float(latitude)
                _data_point.longitude_type = _DOUBLE
                _data_point.longitude = float(point_position.longitude)

            elevation = point_position.elevation
            if elevation is None:
                _data_point.elevation_type = _NULL

            else:
                _data_point.elevation_type = _INT64
                _data_point.elevation = int(elevation)

        value = data_point.value
        if isinstance(value, float):
            _data_point.d = float(value)
            _data_point.value_type = _DOUBLE
            _data_point.value_object = &_data_point.d

        elif isinstance(value, bool):
            _data_point.b = bool(value)
            _data_point.value_type = _BOOLEAN
            _data_point.value_object = &_data_point.b

        elif isinstance(value, int):
            _data_point.i = int(value)
            _data_point.value_type = _INT64
            _data_point.value_object = &_data_point.i

        elif isinstance(value, str):
            _data_point.s = str(value)
            _data_point.value_type = _STRING
            _data_point.value_object = <void*>_data_point.s

        elif isinstance(value, numbers.Integral):
            _data_point.i = int(value)
            _data_point.value_type = _INT64
            _data_point.value_object = &_data_point.i

        elif isinstance(value, numbers.Real):
            _data_point.d = float(value)
            _data_point.value_type = _DOUBLE
            _data_point.value_object = &_data_point.d

        else:
            raise Exception

        yield _data_point


DEF _size = 40000  # (timestamp, float) has length 39


cdef class _Buffer:
    cdef char[_size] _characters
    cdef int _index
    cdef libcpp_string.string _string
    cdef const l_stdint.uint8_t[:] _bytes

    cdef bint add_character(self, char character):
        if self._index == _size:
            return True

        self._characters[self._index] = character
        self._index += 1
        return False

    cdef bytes next_character(self):
        self._index = 0
        return self._characters[:_size]

    cdef bint add_double(self, double d):
        cdef int index
        result = to_chars(self._characters + self._index, self._characters + _size, d)
        if result.ec == <errc>0:
            if result.ptr[-1] == b"f":  # inf
                index = self._index
                self._index = result.ptr - 3 - self._characters
                if self.add_string(b"Infinity"):
                    self._index = index
                    return True

                return False

            self._index = result.ptr - self._characters

            if self._characters[self._index - 1] == b"n":  # nan
                self._characters[self._index - 3] = b"N"
                self._characters[self._index - 1] = b"N"

            return False

        return True

    cdef bytes next_double(self):
        cdef int index = self._index
        self._index = 0
        return self._characters[:index]

    cdef bint add_string(self, libcpp_string.string string):
        if _size < self._index + string.length():
            string.copy(self._characters + self._index, _size - self._index)
            self._string = string.substr(_size - self._index)
            self._index = _size
            return True

        string.copy(self._characters + self._index, string.length())
        self._index += string.length()
        return False

    cdef bytes next_string(self):
        if self._index != _size:
            return None

        cdef bytes _bytes = self._characters[:_size]

        if self._string.length() < _size:
            self._string.copy(self._characters, self._string.length())
            self._index = self._string.length()

        else:
            self._string.copy(self._characters, _size)
            self._string = self._string.substr(_size)

        return _bytes

    @cython.boundscheck(False)
    cdef bint add_bytes(self, const l_stdint.uint8_t[:] bytes):
        if _size < self._index + bytes.shape[0]:
            libc_string.memcpy(self._characters + self._index, &bytes[0], _size - self._index)
            self._bytes = bytes[_size - self._index :]
            self._index = _size
            return True

        libc_string.memcpy(self._characters + self._index, &bytes[0], bytes.shape[0])
        self._index += bytes.shape[0]
        return False

    cdef bytes next_bytes(self):
        if self._index != _size:
            return None

        cdef bytes _bytes = self._characters[:_size]

        if self._bytes.shape[0] < _size:
            libc_string.memcpy(self._characters, &self._bytes[0], self._bytes.shape[0])
            self._index = self._bytes.shape[0]

        else:
            libc_string.memcpy(self._characters, &self._bytes[0], _size)
            self._bytes = self._bytes[_size:]

        return _bytes


def get_line_bytes(_data_points):
    cdef _DataPoint _data_point
    cdef _Buffer buffer = _Buffer()
    cdef bint first
    cdef bytes b

    for _data_point in _data_points:
        if _data_point.equal:
            while buffer.add_character(b"="):
                yield buffer.next_character()

        if _data_point.timestamp_type != _NULL:
            # timestamp
            if buffer.add_string(libcpp_string.to_string(<long long>_data_point.timestamp)):
                while True:
                    b = buffer.next_string()
                    if b is None:
                        break

                    yield b

        # /
        while buffer.add_character(b"/"):
            yield buffer.next_character()

        if _data_point.latitude_type != _NULL:
            # latitude
            if buffer.add_string(libcpp_string.to_string(_data_point.latitude)):
                while True:
                    b = buffer.next_string()
                    if b is None:
                        break

                    yield b

            # :
            while buffer.add_character(b":"):
                yield buffer.next_character()

            # longitude
            if buffer.add_string(libcpp_string.to_string(_data_point.longitude)):
                while True:
                    b = buffer.next_string()
                    if b is None:
                        break

                    yield b

        # /
        while buffer.add_character(b"/"):
            yield buffer.next_character()

        if _data_point.elevation_type != _NULL:
            # elevation
            if buffer.add_string(libcpp_string.to_string(<long long>_data_point.elevation)):
                while True:
                    b = buffer.next_string()
                    if b is None:
                        break

                    yield b

        # space
        while buffer.add_character(b" "):
            yield buffer.next_character()

        if not _data_point.equal:
            # class
            if buffer.add_bytes(u_parse.quote(_data_point.gts_id.class_name).encode()):
                while True:
                    b = buffer.next_bytes()
                    if b is None:
                        break

                    yield b

            # {
            while buffer.add_character(b"{"):
                yield buffer.next_character()

            first = True
            for key_string, value_string in _data_point.gts_id.labels.items():
                if not first:
                    # ,
                    while buffer.add_character(b","):
                        yield buffer.next_character()

                # key
                if buffer.add_bytes(u_parse.quote(key_string).encode()):
                    while True:
                        b = buffer.next_bytes()
                        if b is None:
                            break

                        yield b

                # =
                while buffer.add_character(b"="):
                    yield buffer.next_character()

                # value
                if buffer.add_bytes(u_parse.quote(value_string).encode()):
                    while True:
                        b = buffer.next_bytes()
                        if b is None:
                            break

                        yield b

                first = False

            # } space
            if buffer.add_string(b"} "):
                while True:
                    b = buffer.next_string()
                    if b is None:
                        break

                    yield b

        if _data_point.value_type == _DOUBLE:
            while buffer.add_double(_data_point.d):
                yield buffer.next_double()

        elif _data_point.value_type == _INT64:
            if buffer.add_string(libcpp_string.to_string(<long long>_data_point.i)):
                while True:
                    b = buffer.next_string()
                    if b is None:
                        break

                    yield b

        elif _data_point.value_type == _BOOLEAN:
            while buffer.add_character(b"T" if _data_point.b else b"F"):
                yield buffer.next_character()

        elif _data_point.value_type == _STRING:
            # '
            while buffer.add_character(b"'"):
                yield buffer.next_character()

            # string
            if buffer.add_bytes(u_parse.quote(_data_point.s).encode()):
                while True:
                    b = buffer.next_bytes()
                    if b is None:
                        break

                    yield b

            # '
            while buffer.add_character(b"'"):
                yield buffer.next_character()

        else:
            raise Exception

        # \n
        while buffer.add_character(b"\n"):
            yield buffer.next_character()

    if buffer._index != 0:
        b = buffer._characters[: buffer._index]
        yield b


cdef class _BooleanIterator:
    cdef BooleanIterator _ib
    cdef BooleanIterator _ie
    cdef BooleanChunkedIterator _cb
    cdef BooleanChunkedIterator _ce
    cdef bint _chunked

    def __cinit__(self, array):
        cdef BooleanArray* a
        cdef p_lib.CChunkedArray* c
        if isinstance(array, pyarrow.ChunkedArray):
            c = p_lib.pyarrow_unwrap_chunked_array(array).get()
            self._cb = BooleanChunkedIterator(c[0], 0)
            self._ce = BooleanChunkedIterator(c[0], c[0].length())
            self._chunked = True

        else:
            a = <BooleanArray*>p_lib.pyarrow_unwrap_array(array).get()
            self._ib = a[0].begin()
            self._ie = a[0].end()
            self._chunked = False

    cdef l_optional.optional[libcpp.bool] next(self):
        cdef l_optional.optional[libcpp.bool] b
        if self._chunked:
            b = c_operator.dereference(self._cb)
            c_operator.postincrement(self._cb)

        else:
            b = c_operator.dereference(self._ib)
            c_operator.postincrement(self._ib)

        return b


cdef class _Int64Iterator:
    cdef Int64Iterator _ib
    cdef Int64Iterator _ie
    cdef Int64ChunkedIterator _cb
    cdef Int64ChunkedIterator _ce
    cdef bint _chunked

    def __cinit__(self, array):
        cdef Int64Array* a
        cdef p_lib.CChunkedArray* c
        if isinstance(array, pyarrow.ChunkedArray):
            c = p_lib.pyarrow_unwrap_chunked_array(array).get()
            self._cb = Int64ChunkedIterator(c[0], 0)
            self._ce = Int64ChunkedIterator(c[0], c[0].length())
            self._chunked = True

        else:
            a = <Int64Array*>p_lib.pyarrow_unwrap_array(array).get()
            self._ib = a[0].begin()
            self._ie = a[0].end()
            self._chunked = False

    cdef l_optional.optional[l_stdint.int64_t] next(self):
        cdef l_optional.optional[l_stdint.int64_t] i
        if self._chunked:
            i = c_operator.dereference(self._cb)
            c_operator.postincrement(self._cb)

        else:
            i = c_operator.dereference(self._ib)
            c_operator.postincrement(self._ib)

        return i

    cdef void skip(self):
        if self._chunked:
            c_operator.postincrement(self._cb)

        else:
            c_operator.postincrement(self._ib)


cdef class _DoubleIterator:
    cdef DoubleIterator _ib
    cdef DoubleIterator _ie
    cdef DoubleChunkedIterator _cb
    cdef DoubleChunkedIterator _ce
    cdef bint _chunked

    def __cinit__(self, array):
        cdef DoubleArray* a
        cdef p_lib.CChunkedArray* c
        if isinstance(array, pyarrow.ChunkedArray):
            c = p_lib.pyarrow_unwrap_chunked_array(array).get()
            self._cb = DoubleChunkedIterator(c[0], 0)
            self._ce = DoubleChunkedIterator(c[0], c[0].length())
            self._chunked = True

        else:
            a = <DoubleArray*>p_lib.pyarrow_unwrap_array(array).get()
            self._ib = a[0].begin()
            self._ie = a[0].end()
            self._chunked = False

    cdef l_optional.optional[double] next(self):
        cdef l_optional.optional[double] d
        if self._chunked:
            d = c_operator.dereference(self._cb)
            c_operator.postincrement(self._cb)

        else:
            d = c_operator.dereference(self._ib)
            c_operator.postincrement(self._ib)

        return d

    cdef void skip(self):
        if self._chunked:
            c_operator.postincrement(self._cb)

        else:
            c_operator.postincrement(self._ib)


cdef class _StringIterator:
    cdef StringIterator _ib
    cdef StringIterator _ie
    cdef StringChunkedIterator _cb
    cdef StringChunkedIterator _ce
    cdef bint _chunked

    def __cinit__(self, array):
        cdef StringArray* a
        cdef p_lib.CChunkedArray* c
        if isinstance(array, pyarrow.ChunkedArray):
            c = p_lib.pyarrow_unwrap_chunked_array(array).get()
            self._cb = StringChunkedIterator(c[0], 0)
            self._ce = StringChunkedIterator(c[0], c[0].length())
            self._chunked = True

        else:
            a = <StringArray*>p_lib.pyarrow_unwrap_array(array).get()
            self._ib = a[0].begin()
            self._ie = a[0].end()
            self._chunked = False

    cdef l_optional.optional[string_view] next(self):
        cdef l_optional.optional[string_view] s
        if self._chunked:
            s = c_operator.dereference(self._cb)
            c_operator.postincrement(self._cb)

        else:
            s = c_operator.dereference(self._ib)
            c_operator.postincrement(self._ib)

        return s


cdef class _TimestampIterator:
    cdef TimestampIterator _ib
    cdef TimestampIterator _ie
    cdef TimestampChunkedIterator _cb
    cdef TimestampChunkedIterator _ce
    cdef bint _chunked

    def __cinit__(self, array):
        cdef TimestampArray* a
        cdef p_lib.CChunkedArray* c
        if isinstance(array, pyarrow.ChunkedArray):
            c = p_lib.pyarrow_unwrap_chunked_array(array).get()
            self._cb = TimestampChunkedIterator(c[0], 0)
            self._ce = TimestampChunkedIterator(c[0], c[0].length())
            self._chunked = True

        else:
            a = <TimestampArray*>p_lib.pyarrow_unwrap_array(array).get()
            self._ib = a[0].begin()
            self._ie = a[0].end()
            self._chunked = False

    cdef bint has_next(self):
        if self._chunked:
            return self._cb != self._ce

        return self._ib != self._ie

    cdef l_optional.optional[l_stdint.int64_t] next(self):
        cdef l_optional.optional[l_stdint.int64_t] i
        if self._chunked:
            i = c_operator.dereference(self._cb)
            c_operator.postincrement(self._cb)

        else:
            i = c_operator.dereference(self._ib)
            c_operator.postincrement(self._ib)

        return i

    cdef void skip(self):
        if self._chunked:
            c_operator.postincrement(self._cb)

        else:
            c_operator.postincrement(self._ib)


def get_arrow__data_points(tables):
    cdef _TimestampIterator datetime_iterator
    cdef _DoubleIterator latitude_iterator
    cdef _DoubleIterator longitude_iterator
    cdef _Int64Iterator elevation_iterator
    cdef _BooleanIterator boolean_iterator
    cdef _Int64Iterator int64_iterator
    cdef _DoubleIterator double_iterator
    cdef _StringIterator string_iterator
    cdef l_optional.optional[libcpp.bool] boolean_optional
    cdef l_optional.optional[l_stdint.int64_t] int64_optional
    cdef l_optional.optional[double] double_optional
    cdef l_optional.optional[string_view] string_optional
    cdef bint has_value
    cdef string_view s
    cdef _DataPoint _data_point = _DataPoint()

    for table, column_dictionary in tables:
        datetime_array = table["datetime"].cast(
            target_type=pyarrow.timestamp("us", tz="UTC")
        )
        if len(datetime_array) == 0:
            continue

        latitude_array = table["latitude"].cast(target_type=pyarrow.float64())
        longitude_array = table["longitude"].cast(target_type=pyarrow.float64())
        elevation_array = table["elevation"].cast(target_type=pyarrow.int64())

        for column_name, gts_id in column_dictionary.items():
            datetime_iterator = _TimestampIterator(datetime_array)
            latitude_iterator = _DoubleIterator(latitude_array)
            longitude_iterator = _DoubleIterator(longitude_array)
            elevation_iterator = _Int64Iterator(elevation_array)

            array = table[column_name]
            _data_point.equal = False
            _data_point.gts_id = gts_id

            _array = (
                array.chunk(0) if isinstance(array, pyarrow.ChunkedArray) else array
            )
            if isinstance(_array, pyarrow.BooleanArray):
                _data_point.value_type = _BOOLEAN
                boolean_iterator = _BooleanIterator(array)

            elif isinstance(_array, pyarrow.IntegerArray):
                int64_iterator = _Int64Iterator(array)
                _data_point.value_type = _INT64

            elif isinstance(_array, pyarrow.FloatingPointArray):
                double_iterator = _DoubleIterator(array)
                _data_point.value_type = _DOUBLE

            elif isinstance(_array, pyarrow.StringArray):
                string_iterator = _StringIterator(array)
                _data_point.value_type = _STRING

            else:
                continue

            while datetime_iterator.has_next():
                # value
                if _data_point.value_type == _DOUBLE:
                    double_optional = double_iterator.next()
                    has_value = double_optional.has_value()
                    if has_value:
                        _data_point.d = double_optional.value()

                elif _data_point.value_type == _INT64:
                    int64_optional = int64_iterator.next()
                    has_value = int64_optional.has_value()
                    if has_value:
                        _data_point.i = int64_optional.value()

                elif _data_point.value_type == _BOOLEAN:
                    boolean_optional = boolean_iterator.next()
                    has_value = boolean_optional.has_value()
                    if has_value:
                        _data_point.b = boolean_optional.value()

                elif _data_point.value_type == _STRING:
                    string_optional = string_iterator.next()
                    has_value = string_optional.has_value()
                    if has_value:
                        s = string_optional.value()
                        _data_point.s = bytes(<const char[: s.length()]>s.data()).decode()

                else:
                    has_value = False

                if has_value:
                    # timestamp
                    int64_optional = datetime_iterator.next()
                    if int64_optional.has_value():
                        _data_point.timestamp_type = _TIMESTAMP
                        _data_point.timestamp = int64_optional.value()

                    else:
                        _data_point.timestamp_type = _NULL

                    # latitude
                    double_optional = latitude_iterator.next()
                    if double_optional.has_value():
                        _data_point.latitude_type = _DOUBLE
                        _data_point.latitude = double_optional.value()

                    else:
                        _data_point.latitude_type = _NULL

                    # longitude
                    double_optional = longitude_iterator.next()
                    if double_optional.has_value():
                        _data_point.longitude_type = _DOUBLE
                        _data_point.longitude = double_optional.value()

                    else:
                        _data_point.longitude_type = _NULL

                    # elevation
                    int64_optional = elevation_iterator.next()
                    if int64_optional.has_value():
                        _data_point.elevation_type = _INT64
                        _data_point.elevation = int64_optional.value()

                    else:
                        _data_point.elevation_type = _NULL
                    #

                    yield _data_point
                    _data_point.equal = True

                else:
                    datetime_iterator.skip()
                    latitude_iterator.skip()
                    longitude_iterator.skip()
                    elevation_iterator.skip()
