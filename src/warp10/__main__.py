import warp10
import warp10.warpscript as w_warpscript
import subprocess_shell
import typer
import datetime
import enum
import json
import math
import pathlib
import random
import re
import sys
import tempfile
import time
import typing
import uuid


_typer = typer.Typer()


@_typer.command()
def prepare_standalone(
    directory: typing.Annotated[
        pathlib.Path, typer.Option(help="Path to Warp 10")
    ] = pathlib.Path("/opt/warp10")
):
    """
    Executes `warp10.sh init standalone`.
    """
    initialized = False

    def _update_initialized(string):
        nonlocal initialized

        initialized = initialized or "Configuration files already exist" in string

    _v_ = subprocess_shell.LineStream(_update_initialized, sys.stderr)
    assert [
        directory / "bin/warp10.sh",
        "init",
        "standalone",
    ] >> subprocess_shell.start() >> subprocess_shell.wait(
        stderr=typing.cast(typing.TextIO, _v_), return_codes=None
    ) == 0 or initialized


@_typer.command()
def create_uuid():
    """
    Creates a new UUID.
    """
    print(uuid.uuid1())


class _TokenType(enum.StrEnum):
    read = enum.auto()
    write = enum.auto()


@_typer.command()
def create_token(
    type: typing.Annotated[_TokenType, typer.Argument(help="Type of access token")],
    application: typing.Annotated[
        str, typer.Argument(help="Name of application associated with the token")
    ],
    owner: typing.Annotated[
        uuid.UUID, typer.Argument(help="UUID of the owner associated with the token")
    ],
    owners: typing.Annotated[
        typing.Optional[list],
        typer.Option(
            parser=json.loads,
            metavar="JSON",
            help="List of owners the read token can access",
        ),
    ] = None,
    producers: typing.Annotated[
        typing.Optional[list],
        typer.Option(
            parser=json.loads,
            metavar="JSON",
            help="List of producers the read token can access",
        ),
    ] = None,
    applications: typing.Annotated[
        typing.Optional[list],
        typer.Option(
            parser=json.loads,
            metavar="JSON",
            help="List of applications the read token can access",
        ),
    ] = None,
    id: typing.Optional[str] = None,
    expiry: typing.Annotated[
        typing.Optional[str],
        typer.Option(
            help=(
                r"Expiry point of the token. Either in ISO 8601 or"
                r' "({days}d)?({hours}h)?({minutes}m)?". Default: 1 hour'
            )
        ),
    ] = None,
    attributes: typing.Annotated[
        typing.Optional[dict],
        typer.Option(
            parser=json.loads,
            metavar="JSON",
            help="Map of attributes attached to the token",
        ),
    ] = None,
    labels: typing.Annotated[
        typing.Optional[dict],
        typer.Option(
            parser=json.loads,
            metavar="JSON",
            help=(
                "Map of labels which will be added to the pushed Geo Time Series in the"
                " case of a write token, or map of label selectors which will be added"
                " to the selection criteria for a read token"
            ),
        ),
    ] = None,
    producer: typing.Annotated[
        typing.Optional[uuid.UUID],
        typer.Option(help="UUID of the producer associated with the token"),
    ] = None,
    directory: typing.Annotated[
        pathlib.Path, typer.Option(help="Path to Warp 10")
    ] = pathlib.Path("/opt/warp10"),
):
    """
    Creates a new access token using `warp10.sh tokengen ...`.
    """
    ws = w_warpscript

    dictionary: dict[str, typing.Any] = dict(
        type={_TokenType.read: "READ", _TokenType.write: "WRITE"}[type],
        owner=str(owner),
        producer=str(owner if producer is None else producer),
        application=application,
        issuance=ws.NOW() / ws.ms(1),
        labels={} if labels is None else labels,
        attributes={} if attributes is None else attributes,
    )

    if id is not None:
        dictionary["id"] = id

    expiry_datetime = None
    if expiry is None:
        expiry_datetime = datetime.datetime.utcnow() + datetime.timedelta(hours=1)

    else:
        try:
            expiry_datetime = datetime.datetime.fromisoformat(expiry)

        except Exception:
            match = re.search(
                r"^\s*((?P<days>\d+)d)?\s*((?P<hours>\d+)h)?\s*((?P<minutes>\d+)m)?\s*$",
                expiry,
            )
            if match is None:
                raise ValueError(repr(expiry))

            _v_ = match.group("days", "hours", "minutes")
            days_string, hours_string, minutes_string = _v_

            _v_ = 0 if hours_string is None else int(hours_string)
            _v_ = ((0 if days_string is None else int(days_string)) * 24 + _v_) * 60
            _v_ = _v_ + (0 if minutes_string is None else int(minutes_string))
            dictionary["ttl"] = _v_ * 60_000

    if expiry_datetime is not None:
        dictionary["expiry"] = warp10.get_timestamp(expiry_datetime) // 1_000

    if type == _TokenType.read:
        dictionary["owners"] = [] if owners is None else owners
        dictionary["producers"] = [] if producers is None else producers
        dictionary["applications"] = [] if applications is None else applications

    with tempfile.NamedTemporaryFile() as file:
        file.write(str(ws.TOKENGEN(dictionary)).encode())
        file.flush()

        _v_ = [directory / "bin/warp10.sh", "tokengen", file.name]
        stdout_string = _v_ >> subprocess_shell.start() >> subprocess_shell.read()

    (dictionary,) = json.loads(typing.cast(str, stdout_string))
    ident_string = dictionary["ident"]
    token_string = dictionary["token"]
    print(f"ident: {ident_string}")
    print(f"token: {token_string}")


@_typer.command()
def benchmark(
    write_token: str,
    read_token: str,
    n: typing.Annotated[int, typer.Option(help="Number of data points")] = int(1e6),
    _2d: typing.Annotated[
        bool, typer.Option("--2d/--no-2d", help="Use 2-dimensional types")
    ] = True,
    delete: typing.Annotated[bool, typer.Option(help="Delete Geo Time Series")] = True,
    create: typing.Annotated[bool, typer.Option(help="Create Geo Time Series")] = True,
    class_: typing.Annotated[str, typer.Option("--class")] = "_benchmark",
    address: str = warp10.DEFAULT_ADDRESS,
    port: int = warp10.DEFAULT_PORT,
):
    if _2d:
        import warp10.arrow as w_arrow
        import warp10.pandas as w_pandas
        import numpy.random as n_random
        import pandas
        import pandas.testing as p_testing
        import pyarrow

    else:
        w_arrow = None
        w_pandas = None
        n_random = None
        pandas = None
        p_testing = None
        pyarrow = None

    now = warp10.get_utc(datetime.datetime.utcnow()) - datetime.timedelta(days=1)
    gts_pattern = warp10.GtsPattern(f"^{re.escape(class_)}$")

    if delete:
        print("! delete Geo Time Series")
        print("..", end="", flush=True)

        start_time = time.monotonic()
        list(warp10.delete_data_points(gts_pattern, write_token))
        end_time = time.monotonic()

        print(" done")
        print(f"in {end_time - start_time:0.1f} s")

    data_points = None
    data_frame = None

    if create:
        gts_id = warp10.GtsId(class_, {})

        print("! create Geo Time Series")

        d = datetime.timedelta(microseconds=1)
        if _2d:
            pandas = typing.cast(typing.Any, pandas)
            print("creating 2d object ..", end="", flush=True)

            _v_ = pandas.date_range(end=now, periods=n, freq=d, unit="us")[::-1]
            data_frame = pandas.DataFrame({"datetime": _v_})

            data_frame["latitude"] = math.nan
            data_frame["longitude"] = math.nan

            _v_ = pandas.Series([pandas.NA], dtype=pandas.Int64Dtype())
            data_frame["elevation"] = _v_

            _v_ = typing.cast(typing.Any, n_random).default_rng().random(n)
            data_frame[gts_id] = _v_

        else:
            print("creating DataPoint objects ..", end="", flush=True)
            data_points = [
                warp10.DataPoint(now - index * d, None, gts_id, random.random())
                for index in range(n)
            ]

        print(" done")
        print("adding data points ..", end="", flush=True)

        start_time = time.monotonic()

        if _2d:
            _v_ = typing.cast(typing.Any, pyarrow).Table.from_pandas(data_frame)
            _data_points = [(_v_, {str(gts_id): gts_id})]

        else:
            _data_points = data_points

        _v_ = typing.cast(typing.Any, _data_points)
        warp10.add_data_points(_v_, write_token, arrow=_2d, address=address, port=port)

        end_time = time.monotonic()

        print(" done")

        _v_ = n / (end_time - start_time)
        print(f"in {end_time - start_time:0.1f} s ({_v_:0.1f} points/s)")

    print("! fetch Geo Time Series")
    print("..", end="", flush=True)

    start_time = time.monotonic()

    generator = warp10.get_data_points(gts_pattern, read_token, start=n, build=_2d)
    got_data_points = None
    got_data_frame = None

    if _2d:
        (table,) = typing.cast(typing.Any, w_arrow).merge_tables(generator)
        got_data_frame = table.to_pandas()
        del table

        _v_ = {
            (
                name
                if name in ("datetime", "latitude", "longitude", "elevation")
                else eval(name, dict(GtsId=warp10.GtsId))
            ): series
            for name, series in got_data_frame.items()
        }
        got_data_frame = typing.cast(typing.Any, w_pandas)._create_data_frame(_v_)

    else:
        got_data_points = list(generator)

    end_time = time.monotonic()

    print(" done")

    if create:
        if _2d:
            _v_ = typing.cast(typing.Any, p_testing)
            _v_.assert_frame_equal(got_data_frame, data_frame)

        else:
            assert got_data_points == data_points

    _v_ = n / (end_time - start_time)
    print(f"in {end_time - start_time:0.1f} s ({_v_:0.1f} points/s)")


if __name__ == "__main__":
    _typer()
