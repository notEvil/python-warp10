import warp10.warpscript as ws
import datetime


print()
print(((ws.c(1) + 2) * 3).int() / 4)

# `c` is a special function to cast Python objects to WarpScript types.
# In most cases, this is not necessary.

# `.int` is the dunder method __int__ provided as regular method.
# Many dunder methods are provided as regular methods because Python enforces
# the respective return types.

now = datetime.datetime.now()

print()
print(now)

print()
print(
    ws.BUCKETIZE(
        ws.Plus([
            ws.FETCH(
                "token",
                "~class",
                dict(lab="~el"),
                ws.TOTIMESTAMP(now),
                datetime.timedelta(hours=1),
            )
        ]),
        ws.bucketizer_mean(),
        ws.NOW(),
        ws.m(1),
        60,
    )
)

# `Plus` and `Star` are special containers (tuples) to make it explicit that
# the function consumes  Plus: one or more  Star: zero or more  arguments from
# the stack.

# `datetime.datetime` is treated like STRING
# `datetime.timedelta` is treated like LONG
